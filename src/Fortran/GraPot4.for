C   IMSL ROUTINE NAME   - MMBSJ0                                        MMBD0010
C                                                                       MMBD0020
C-----------------------------------------------------------------------MMBD0030
C                                                                       MMBD0040
C   COMPUTER            - IBM/DOUBLE                                    MMBD0050
C                                                                       MMBD0060
C   LATEST REVISION     - JUNE 1, 1981                                  MMBD0070
C                                                                       MMBD0080
C   PURPOSE             - BESSEL FUNCTION OF THE FIRST KIND OF ORDER    MMBD0090
C                           ZERO                                        MMBD0100
C                                                                       MMBD0110
C   USAGE               - FUNCTION MMBSJ0 (ARG,IER)                     MMBD0120
C                                                                       MMBD0130
C   ARGUMENTS    MMBSJ0 - OUTPUT VALUE OF THE FUNCTION AT ARG. MMBSJ0   MMBD0140
C                           MUST BE TYPED APPROPRIATELY IN THE CALLING  MMBD0150
C                           PROGRAM. (SEE THE PRECISION/HARDWARE        MMBD0160
C                           SECTION.)                                   MMBD0170
C                ARG    - INPUT ARGUMENT. THE ABSOLUTE VALUE OF ARG MUSTMMBD0180
C                           BE LESS THAN OR EQUAL TO XMAX, WHICH IS OF  MMBD0190
C                           THE ORDER OF 10**8. THE EXACT VALUE OF XMAX MMBD0200
C                           MAY ALLOW LARGER RANGES FOR ARG ON SOME     MMBD0210
C                           COMPUTERS. SEE THE PROGRAMMING NOTES IN THE MMBD0220
C                           MANUAL FOR THE EXACT VALUES.                MMBD0230
C                IER    - ERROR PARAMETER. (OUTPUT)                     MMBD0240
C                         TERMINAL ERROR                                MMBD0250
C                           IER = 129 INDICATES THAT THE ABSOLUTE VALUE MMBD0260
C                             OF ARG IS GREATER THAN XMAX. MMBSJ0 IS    MMBD0270
C                             SET TO ZERO.                              MMBD0280
C                                                                       MMBD0290
C   PRECISION/HARDWARE  - DOUBLE/H32,H36                                MMBD0300
C                       - SINGLE/H48,H60                                MMBD0310
C                                                                       MMBD0320
C   REQD. IMSL ROUTINES - UERTST,UGETIO                                 MMBD0330
C                                                                       MMBD0340
C   NOTATION            - INFORMATION ON SPECIAL NOTATION AND           MMBD0350
C                           CONVENTIONS IS AVAILABLE IN THE MANUAL      MMBD0360
C                           INTRODUCTION OR THROUGH IMSL ROUTINE UHELP  MMBD0370
C                                                                       MMBD0380
C   COPYRIGHT           - 1978 BY IMSL, INC. ALL RIGHTS RESERVED.       MMBD0390
C                                                                       MMBD0400
C   WARRANTY            - IMSL WARRANTS ONLY THAT IMSL TESTING HAS BEEN MMBD0410
C                           APPLIED TO THIS CODE. NO OTHER WARRANTY,    MMBD0420
C                           EXPRESSED OR IMPLIED, IS APPLICABLE.        MMBD0430
C                                                                       MMBD0440
C-----------------------------------------------------------------------MMBD0450
C                                                                       MMBD0460
      real*8 FUNCTION MMBSJ0 (ARG,IER)                                 ! MMBD0470
C                                  SPECIFICATIONS FOR ARGUMENTS         MMBD0480
      INTEGER            IER                                            !MMBD0490
      real*8   ARG                                                      !MMBD0500
C                                  SPECIFICATIONS FOR LOCAL VARIABLES   !MMBD0510
      real*8   AX,DOWN,FUDGE,FUDGEX,PI2,PROD,                           !MMBD0520
     1                   PROD1,PRP,PRQ,P0,P1,QRP,QRQ,Q0,Q1,R0,R1,TWOPI1,!MMBD0530
     2                   TWOPI2,U,UP,W,X,XDEN,XMAX,XMAX1,XNUM,XSMALL,   !MMBD0540
     3                   X01,X01P,X02,X11,X12,Z,ZSQ                     !MMBD0550
      real*8   DABS,DCOS,DSIN,DSQRT                                     !MMBD0560
      DIMENSION          PRP(7),PRQ(8),P0(6),P1(6),QRP(5),QRQ(7),Q0(5), !MMBD0570
     1                   Q1(5)                                          !MMBD0580
C                                  MACHINE DEPENDENT CONSTANTS          !MMBD0590
C                                     FUDGE = 7 * 16**(-15)             !MMBD0600
C                                     FUDGEX = 2 * 16**(-15)            !MMBD0610
C                                     PI2 = 2 / PI                      !MMBD0620
C                                     TWOPI1 + TWOPI2 = 2*PI TO EXTRA   !MMBD0630
C                                     PRECISION                         !MMBD0640
C                                     XMAX = 16**9, LARGEST ACCEPTABLE  !MMBD0650
C                                     ARGUMENT                          !MMBD0660
C                                     XMAX1 = SMALLEST FLOATING-POINT   !MMBD0670
C                                     CONSTANT WITH ENTIRELY INTEGER    !MMBD0680
C                                     REPRESENTATION                    !MMBD0690
C                                     XSMALL = 16**(-10), ARGUMENT      !MMBD0700
C                                     BELOW WHICH J0 MAY BE REPRESENTED !MMBD0710
C                                     BY ONE TERM IN THE ASCENDING      !MMBD0720
C                                     SERIES                            !MMBD0730
C                                     X01 + X02 = FIRST ZERO OF J-SUB-0 !MMBD0740
C                                     TO EXTRA PRECISION                !MMBD0750
C                                     X11 + X12 = SECOND ZERO OF        !MMBD0760
C                                     J-SUB-0 TO EXTRA PRECISION        !MMBD0770
C                                                                       !MMBD0780
C      DATA PI2/Z40A2F9836E4E4415/,X01P/Z414CF454BA5C6CFF/,             ! MMBD0790
C     1     XMAX/Z4A10000000000000/,XSMALL/Z3710000000000000/,          ! MMBD0800
C     2     X01/Z41267A2A5D2E367F/,X02/Z33785631412ED80C/,              ! MMBD0810
C     3     X11/Z4158523D6CB0B914/,X12/Z335D415335829085/,              ! MMBD0820
C     4     FUDGE/Z3F00000000000007/,FUDGEX/Z3F00000000000002/,         ! MMBD0830
C     5     XMAX1/Z4E10000000000000/,TWOPI1/Z416487ED00000000/,         ! MMBD0840
C     6     TWOPI2/Z3B5110B4611A6263/                                   ! MMBD0850
C                                                                       !MMBD0860
C                                  COEFFICIENTS FOR RATIONAL            !MMBD0870
C                                    APPROXIMATION OF J-0(X) / (X**2 -  !MMBD0880
C                                    X0**2), XSMALL .LT. ABS(X) .LE.    !MMBD0890
C                                    4.0                                !MMBD0900
C                                                                       !MMBD0910
C      DATA PRP/Z46CA573794BA3CCD, ZC84A13D638788682, Z49CB45410CD363E2,! MMBD0920
C     1         ZCAC04FD96CA3D13C, ZC03E0A0AA9070ECC, Z42CEE26BBA894F91,! MMBD0930
C     2         ZC511E2BA11A7964F/                                      ! MMBD0940
C      DATA QRP/Z461BD6FBD95604E5, Z482157D376E3FA41, Z4A188521B2737048,! MMBD0950
C     1         Z4B8B059E13844EA9, Z43EA08F96DC34160/                   ! MMBD0960
C                                                                       !MMBD0970
C                                  COEFFICIENTS FOR RATIONAL            !MMBD0980
C                                    APPROXIMATION OF J-0(X) / (X**2 -  !MMBD0990
C                                    X1**2), 4.0 .LT. ABS(X) .LE. 8.0   !MMBD1000
C                                                                       !MMBD1010
C      DATA PRQ/Z44228357665284F0, Z445B9A17B1FAF674, Z4450CBD23F9D18F8,! MMBD1020
C     1         ZC438EFF0C424693A, ZC45FBC2804C4AD24, ZC3E4FE12D107D8FE,! MMBD1030
C     2         Z42612EF3BE05308B, Z435CE6C86E9C94C4/                   ! MMBD1040
C      DATA QRQ/Z4329A256E60E8861, ZC41703C0D8DD844D, Z4491F1FAE2602AF6,! MMBD1050
C     1         ZC5290AE200BF3249, Z45781CE0B959DFC0, ZC5AEB958F7E70373,! MMBD1060
C     2         ZC232842291AC3C0F/                                      ! MMBD1070
C                                  COEFFICIENTS FOR HART                !MMBD1080
C                                    APPROXIMATION, ABS(X) .GT. 8.0     !MMBD1090
C                                                                       !MMBD1100
C      DATA P0/Z43D98A60D8DF24C3, Z4452B285FC49D24A, Z44A18162FACFC2BA, ! MMBD1110
C     1        Z4458FB17172BA9CE, Z40E3BDD722D5C557, Z4299C313AEDED625/ ! MMBD1120
C      DATA Q0/Z43DAEDF9E9A18CC3, Z4452DF59BE6C6252, Z44A19A69994E46FC, ! MMBD1130
C     1        Z4458FB17172BA9CE, Z429D1C91B97EDABA/                    ! MMBD1140
C      DATA P1/ZC2164CDDF2D4A1E8, ZC26FD594A1F7CFF9, ZC2B9EB66BD880301, ! MMBD1150
C     1        ZC2593A027883B414, ZBF240EF5FC15AF42, ZC113E7D834C33D98/ ! MMBD1160
C      DATA Q1/Z435D0B91E9A98D9F, Z441C60472C1DEFDE, Z442EAF21ACD49DAB, ! MMBD1170
C     1        Z44164E809E20ED05, Z425A980148BFDACA/                    ! MMBD1180
      DATA  PI2/0.636619772367581341/,X01P/4.80965111539154555/
      DATA XMAX/68719476736.0000000/,XSMALL/0.909494701772928238E-12/
      DATA X01/2.40482555769577266/,X02/0.104375439771945442E-15/
      DATA X11/5.52007811028631057/,X12/0.808859714614641942E-16/
      DATA FUDGE/0.607153216591882483E-17/
      DATA FUDGEX/0.173472347597680709E-17/
      DATA XMAX1/4503599627370496.00/
      DATA TWOPI1/6.28318500518798828/
      DATA TWOPI2/0.301991598195675284E-06/
      DATA  PRP/13260599.5809667588,-1242814008.47080243,
     +        54565015757.2118855,-825973370019.817322,
     +        -0.242340723291870563,206.884456308863779,
     +      -73259.6293102141790/
      DATA  QRP/1824507.84896879760,559403894.890537322,
     +      105312793203.438599,9553515198532.91626,
     +      3744.56089569350843/
      DATA  PRQ/8835.34140506501717,23450.0925595142071,
     +      20683.8212831674537,-14575.9404928929234,
     +      -24508.1563227579791,-3663.87959387841681,
     +         97.1834067118329976,1486.42393361248492/
      DATA  QRQ/666.146215492981412,-5891.75330910186767,
     +      37361.9800167183766,-168110.125182339121,
     +      491982.045251726173,-715669.560523045991,
     +         -50.5161524816031111/
c      DATA  P0/3480.64864432492703,21170.5233808649446,
c     +      41345.3866395807654,
c     +         22779.0901973046839,0.889615484242104557,
c     +      153.762019090083545/
c      DATA  Q0/3502.87351382356081,21215.3505618801155,
c     +      41370.4124955104162,
c     +         22779.0901973046839,157.111598580808938/
c      DATA  P1/-22.3002616662141975,-111.834299204827378,
c     +      -185.919536443429937,
c     +   -89.2266002008000925,-0.880333030486807507E-02,
c     +   -1.24410267458356394/
c      DATA  Q1/1488.72312322837564,7264.27801692110188,
c     +    11951.1315434346134,
c     +       5710.50241285120592,90.5937695949931268/
      data  P0/3480.64864432492703,21170.5233808649446,
     +         41345.3866395807654,22779.0901973046839,
     +         0.889615484242104557,153.762019090083545/
      data  Q0/3502.87351382356081,21215.3505618801155,
     +         41370.4124955104162,22779.0901973046839,
     +         157.111598580808938/
      data  P1/-22.3002616662141975,-111.834299204827378,
     +         -185.919536443429937,-89.2266002008000925,
     +         -0.880333030486807507E-02,-1.24410267458356394/
      data  Q1/1488.72312322837564,7264.27801692110188,
     +         11951.1315434346134,5710.50241285120592,
     +         90.5937695949931268/

C                                  FIRST EXECUTABLE STATEMENT           !MMBD1190
      IER = 0                                                           !MMBD1200
      AX = DABS(ARG)                                                    !MMBD1210
      IF (AX.GT.XMAX) GO TO 35                                          !MMBD1220
      IF (AX.GT.XSMALL) GO TO 5                                         !MMBD1230
      MMBSJ0 = 1.0D0                                                    !MMBD1240
      GO TO 9005                                                        !MMBD1250
    5 IF (AX.GT.8.0D0) GO TO 25                                         !MMBD1260
      IF (AX.GT.4.0D0) GO TO 15                                         !MMBD1270
C                                  XSMALL .LT. ABS(X) .LE. 4.0,         !MMBD1280
C                                    CALCULATION SCALED TO AVOID LOSS   !MMBD1290
C                                    OF ACCURACY ASSOCIATED WITH        !MMBD1300
C                                    HEXADECIMAL ARITHMETIC             !MMBD1310
      ZSQ = AX*AX                                                       !MMBD1320
      XNUM = (PRP(5)*ZSQ+PRP(6))*ZSQ+PRP(7)                             !MMBD1330
      XDEN = 4.0D0*ZSQ+QRP(5)                                           !MMBD1340
      DO 10 I=1,4                                                       !MMBD1350
         XNUM = XNUM*ZSQ+PRP(I)                                         !MMBD1360
         XDEN = XDEN*ZSQ+QRP(I)                                         !MMBD1370
   10 CONTINUE                                                          !MMBD1380
C                                  CALCULATION TO PRESERVE ACCURACY     !MMBD1390
C                                    NEAR THE FIRST ZERO OF J-0         !MMBD1400
      PROD = (AX-X01)-X02                                               !MMBD1410
      PROD1 = AX+AX+X01P                                                !MMBD1420
      R0 = (PROD1*XNUM)/XDEN                                            !MMBD1430
      R0 = R0-FUDGE*PROD1                                               !MMBD1440
      MMBSJ0 = PROD*R0                                                  !MMBD1450
      GO TO 9005                                                        !MMBD1460
C                                  4.0 .LT. ABS(X) .LE. 8.0,            !MMBD1470
C                                    CALCULATION SCALED TO AVOID LOSS   !MMBD1480
C                                    OF ACCURACY ASSOCIATED WITH        !MMBD1490
C                                    HEXADECIMAL ARITHMETIC             !MMBD1500
   15 ZSQ = 1.0D0-(AX*AX)/64.0D0                                        !MMBD1510
      XNUM = PRQ(7)*ZSQ+PRQ(8)                                          !MMBD1520
      XDEN = 2.0D0*ZSQ+QRQ(7)                                           !MMBD1530
      DO 20 I=1,6                                                       !MMBD1540
         XNUM = XNUM*ZSQ+PRQ(I)                                         !MMBD1550
         XDEN = XDEN*ZSQ+QRQ(I)                                         !MMBD1560
   20 CONTINUE                                                          !MMBD1570
      R0 = XNUM/XDEN                                                    !MMBD1580
      R0 = R0+FUDGEX                                                    !MMBD1590
C                                  CALCULATION TO PRESERVE ACCURACY     !MMBD1600
C                                    NEAR THE SECOND ZERO OF J-0        !MMBD1610
      PROD = (AX+X11)                                                   !MMBD1620
      PROD = (AX-X11)*PROD-(PROD*X12)                                   !MMBD1630
      MMBSJ0 = R0*PROD                                                  !MMBD1640
      GO TO 9005                                                        !MMBD1650
C                                  ABS(X) .GT. 8.0                      !MMBD1660
   25 Z = 8.0D0/AX                                                      !MMBD1670
      W = AX/TWOPI1                                                     !MMBD1680
c      WRITE(6,*) ' W',W
c      WRITE(6,*)(W+XMAX1)
c      WRITE(6,*) ((W+XMAX1)-XMAX1)
C      W = ((W+XMAX1)-XMAX1)+0.125D0                                    ! MMBD1690
      W = INT(W)+0.125D0                                                !MMBD1690
c      WRITE(6,*) ' W',W
c      WRITE(6,*)  (AX-W*TWOPI1)
      U = (AX-W*TWOPI1)-W*TWOPI2                                        !MMBD1700
c      WRITE(6,*) ' U',U
      ZSQ = Z*Z                                                         !MMBD1710
      XNUM = P0(5)*ZSQ+P0(6)                                            !MMBD1720
      XDEN = ZSQ+Q0(5)                                                  !MMBD1730
      UP = P1(5)*ZSQ+P1(6)                                              !MMBD1740
      DOWN = ZSQ+Q1(5)                                                  !MMBD1750
      DO 30 I=1,4                                                       !MMBD1760
         XNUM = XNUM*ZSQ+P0(I)                                          !MMBD1770
         XDEN = XDEN*ZSQ+Q0(I)                                          !MMBD1780
         UP = UP*ZSQ+P1(I)                                              !MMBD1790
         DOWN = DOWN*ZSQ+Q1(I)                                          !MMBD1800
   30 CONTINUE                                                          !MMBD1810
      R0 = XNUM/XDEN                                                    !MMBD1820
      R1 = UP/DOWN                                                      !MMBD1830
      MMBSJ0 = DSQRT(PI2/AX)*(R0*DCOS(U)-Z*R1*DSIN(U))                  !MMBD1840
      GO TO 9005                                                        !MMBD1850
C                                  ERROR RETURN FOR ABS(X) .GT. XMAX    !MMBD1860
   35 MMBSJ0 = 0.0D0                                                    !MMBD1870
      IER = 129                                                         !MMBD1880
 9000 CONTINUE                                                          !MMBD1890
C      CALL UERTST (IER,6HMMBSJ0)                                       ! MMBD1900
 9005 RETURN                                                            !MMBD1910
      END                                                               !MMBD1920
