C   IMSL ROUTINE NAME   - MMBSJ1                                        MMBI0010
C                                                                       MMBI0020
C-----------------------------------------------------------------------MMBI0030
C                                                                       MMBI0040
C   COMPUTER            - IBM/DOUBLE                                    MMBI0050
C                                                                       MMBI0060
C   LATEST REVISION     - JUNE 1, 1981                                  MMBI0070
C                                                                       MMBI0080
C   PURPOSE             - BESSEL FUNCTION OF THE FIRST KIND OF ORDER    MMBI0090
C                           ONE                                         MMBI0100
C                                                                       MMBI0110
C   USAGE               - FUNCTION MMBSJ1 (ARG,IER)                     MMBI0120
C                                                                       MMBI0130
C   ARGUMENTS    MMBSJ1 - OUTPUT VALUE OF THE FUNCTION AT ARG. MMBSJ1   MMBI0140
C                           MUST BE TYPED APPROPRIATELY IN THE CALLING  MMBI0150
C                           PROGRAM. (SEE THE PRECISION/HARDWARE        MMBI0160
C                           SECTION.)                                   MMBI0170
C                ARG    - INPUT ARGUMENT. THE ABSOLUTE VALUE OF ARG     MMBI0180
C                           MUST BE LESS THAN OR EQUAL TO XMAX, WHICH   MMBI0190
C                           IS OF THE ORDER OF 10**8. THE EXACT VALUE OFMMBI0200
C                           XMAX MAY ALLOW LARGER RANGES FOR ARG ON SOMEMMBI0210
C                           COMPUTERS. SEE THE PROGRAMMING NOTES IN THE MMBI0220
C                           MANUAL FOR THE EXACT VALUES.                MMBI0230
C                IER    - ERROR PARAMETER. (OUTPUT)                     MMBI0240
C                         TERMINAL ERROR                                MMBI0250
C                           IER = 129 INDICATES THAT THE ABSOLUTE VALUE MMBI0260
C                             OF ARG IS GREATER THAN XMAX. MMBSJ1 IS    MMBI0270
C                             SET TO ZERO.                              MMBI0280
C                                                                       MMBI0290
C   PRECISION/HARDWARE  - DOUBLE/H32,H36                                MMBI0300
C                       - SINGLE/H48,H60                                MMBI0310
C                                                                       MMBI0320
C   REQD. IMSL ROUTINES - UERTST,UGETIO                                 MMBI0330
C                                                                       MMBI0340
C   NOTATION            - INFORMATION ON SPECIAL NOTATION AND           MMBI0350
C                           CONVENTIONS IS AVAILABLE IN THE MANUAL      MMBI0360
C                           INTRODUCTION OR THROUGH IMSL ROUTINE UHELP  MMBI0370
C                                                                       MMBI0380
C   COPYRIGHT           - 1978 BY IMSL, INC. ALL RIGHTS RESERVED.       MMBI0390
C                                                                       MMBI0400
C   WARRANTY            - IMSL WARRANTS ONLY THAT IMSL TESTING HAS BEEN MMBI0410
C                           APPLIED TO THIS CODE. NO OTHER WARRANTY,    MMBI0420
C                           EXPRESSED OR IMPLIED, IS APPLICABLE.        MMBI0430
C                                                                       MMBI0440
C-----------------------------------------------------------------------MMBI0450
C                                                                       MMBI0460
      DOUBLE PRECISION FUNCTION MMBSJ1 (ARG,IER)                        !MMBI0470
C                                  SPECIFICATIONS FOR ARGUMENTS         !MMBI0480
      INTEGER            IER                                            !MMBI0490
      DOUBLE PRECISION   ARG                                            !MMBI0500
C                                  SPECIFICATIONS FOR LOCAL VARIABLES   !MMBI0510
      DOUBLE PRECISION   AP1,AP2,AQ1,AQ2,AX,B,D,DEN,DEN2,               !MMBI0520
     1                   P,Q,RTPI2,TWOPI1,TWOPI2,V,X,XC,XMAX,XMAX1,     !MMBI0530
     2                   XMIN,XNUM,XNUM2,XSMALL,X01,X02,X11,X12,Y,Z,ZSQ !MMBI0540
      DOUBLE PRECISION   DABS,DCOS,DSIN,DSQRT                           !MMBI0550
      DIMENSION          P(6),Q(5),B(8),D(6),AP1(6),AQ1(5),AP2(6),      !MMBI0560
     1                   AQ2(5)                                         !MMBI0570
C                                  MACHINE DEPENDENT CONSTANTS          !MMBI0580
C                                     RTPI2 = SQRT(2/PI)                !MMBI0590
C                                     TWOPI1 + TWOPI2 = 2*PI TO EXTRA   !MMBI0600
C                                     PRECISION                         !MMBI0610
C                                     XMAX = 16**9, LARGEST ACCEPTABLE  !MMBI0620
C                                     ARGUMENT                          !MMBI0630
C                                     XMAX1 = SMALLEST FLOATING-POINT   !MMBI0640
C                                     CONSTANT WITH ENTIRELY INTEGER    !MMBI0650
C                                     REPRESENTATION                    !MMBI0660
C                                     XMIN = 2*16**(-65), ARGUMENT      !MMBI0670
C                                     BELOW WHICH J1 MAY BE REPRESENTED !MMBI0680
C                                     BY ONE TERM IN THE ASCENDING      !MMBI0690
C                                     SERIES                            !MMBI0700
C                                     X01 + X02 = FIRST ZERO OF J-SUB-1 !MMBI0710
C                                     TO EXTRA PRECISION                !MMBI0720
C                                     X11 + X12 = SECOND ZERO OF        !MMBI0730
C                                     J-SUB-1 TO EXTRA PRECISION        !MMBI0740
C                                                                       !MMBI0750
c      DATA RTPI2/Z40CC42299EA1B284/,XMAX1/Z4E10000000000000/,          ! MMBI0760
c     1     TWOPI1/Z416487ED00000000/,TWOPI2/Z3B5110B4611A6263/,        ! MMBI0770
c     2     XMAX/Z4A10000000000000/,XMIN/Z0020000000000000/,            ! MMBI0780
c     3     X01/Z413D4EAAEB5EDE14/,X02/ZB42B00AAD4E8D905/,              ! MMBI0790
c     4     X11/Z41703FD7CED1C942/,X12/ZB426C89B67490DC3/,              ! MMBI0800
c     5     XSMALL/Z3710000000000000/                                   ! MMBI0810
C                                                                       !MMBI0820
C                                  COEFFICIENTS FOR RATIONAL            !MMBI0830
C                                     APPROXIMATION OF J-1(ARG) / (ARG* !MMBI0840
C                                     (ARG**2 - X0**2)), XSMALL .LT.    !MMBI0850
C                                     ABS(ARG) .LE. 4.0                 !MMBI0860
C                                                                       !MMBI0870
c      DATA P/Z4536DD559B0B7C0C,ZC71D043A883CE853,Z4871DE158C64455A,    ! MMBI0880
c     1       ZC99F464358B426A7,ZC331E92085C3C546,Z41121DAB242E9189/    ! MMBI0890
c      DATA Q/Z45521EA694F69585,Z4756D6B7BF375C36,Z49391785AE89FEA2,    ! MMBI0900
c     1       Z4B1244ED1117566A,Z4331AB2176B94DAB/                      ! MMBI0910
C                                  COEFFICIENTS FOR RATIONAL            !MMBI0920
C                                     APPROXIMATION OF J-1(ARG) / (ARG* !MMBI0930
C                                     (ARG**2 - X1**2)), 4.0 .LT.       !MMBI0940
C                                     ABS(ARG) .LE. 8.0 NUMERATOR IN    !MMBI0950
C                                     MINI-NEWTON FORM                  !MMBI0960
C                                                                       !MMBI0970
c      DATA B/Z3F16D4006B0D22BD,ZC180DE0853CAFA1B,Z4412D36A5DC52FD4,    ! MMBI0980
c     1       ZC61695D15E8962F9,Z47DED253A2219B17,ZC93AB436A6806296,    ! MMBI0990
c     2       Z4A188AAE4A7E0460,Z4ADF87033A5B9DA8/                      ! MMBI1000
c      DATA D/Z43278F21F064AB34,Z456E52DAAD0A7BF8,Z47D0D51111DA72EA,    ! MMBI1010
c     1       Z4A1115950FE9E4F1,Z4BE5172B5C19336D,Z4D5E5B25E8D16D8C/    ! MMBI1020
C                                                                       !MMBI1030
C                                  COEFFICIENTS FOR HART                !MMBI1040
C                                     APPROXIMATION, ABS(ARG) .GT. 8.0  !MMBI1050
C                                                                       !MMBI1060
c      DATA AP1/Z42D32725A71ABCBC,Z4413797BB3646E7F,Z447A79F68E6FDE75,  ! MMBI1070
c     1         Z44F526D8621E32BD,Z448998AA37C33226,Z41141D6010A865E8/  ! MMBI1080
c      DATA AQ1/Z42CB13D8478E3359,Z44134265806168F6,Z447A086808A4FBD0,  ! MMBI1090
c     1         Z44F4E658D2540F48,Z448998AA37C33225/                    ! MMBI1100
c      DATA AP2/Z4149173B22770030,Z4253309D024E9158,Z431A9FCBFC4E8E2C,  ! MMBI1110
c     1        Z432D10A07BB8D1D0,Z4315F2CD95877EA5,Z3F90B4834E29AFD8/   ! MMBI1120
c      DATA AQ2/Z4267D19A2C564CE7,Z437132FCB9B7E7E8,Z4423C03B50CA65EA,  ! MMBI1130
c     1         Z443C362D661B27E3,Z441D43BCC75F5387/                    ! MMBI1140
       DATA RTPI2/0.797884560802865350/
       DATA XMAX1/4503599627370496.00/
       DATA TWOPI1/6.28318500518798828/
       DATA TWOPI2/0.301991598195675284E-06/
       DATA XMAX/68719476736.0000000/
ccc       DATA XMIN/0.107952106938680558E-77/
       DATA XMIN/0.107952106938680558E-33/
       DATA X01/3.83170597020751291/
       DATA X02/-0.596781050750941411E-15/
       DATA X11/7.01558666981561929/
       DATA X12/-0.538230866384163017E-15/
       DATA XSMAll/0.909494701772928238E-12/
       DATA P/224725.350352749403,-30426024.5148699991,
     +       1910379916.39168322,-42754913675.2594366,
     +      -798.570440067986397,1.13224329122121703/
       DATA Q/336362.411367973385,91057019.9510156736,
     +      15325485800.6246662,1255452774773.40088,
     +      794.695669864503259/
       DATA B/0.557327426287973233E-02,-8.05420716029732975,
     +        4819.41549332063369,-1480145.36928385333,
     +        233645370.133204546,-15758223976.0240688,
     +        105405893246.017090,960042842715.615845/
       DATA D/632.945785897481301,451885.667246326688,
     +       218976529.115832247,73376534505.8943024,
     +       15742979654035.2141,1659929136797400.75/
       DATA AP1/211.152918285396240,4985.48320605943354,
     +       31353.9631109159573,       62758.8452471612809,
     +   35224.6649133679803,       1.25717169291453423/
       DATA AQ1/  203.077518913475931,       4930.39649018108867,
     +       31240.4063819041039,       62694.3469593560512,
     +   35224.6649133679794/
       DATA AP2/  4.56817162955122669,       83.1898957673850816,
     +       425.987301165444251,       721.039180490447507,
     +   351.175191430355255,      0.353284005274012367E-01/
       DATA AQ2/  103.818758746213373,       1811.18670055235134,
     +       9152.23170151699196,       15414.1773392650966,
     +   7491.73741718091242/

C                                  FIRST EXECUTABLE STATEMENT           !MMBI1150
      IER = 0                                                           !MMBI1160
      AX = DABS(ARG)                                                    !MMBI1170
      IF (AX.GT.XMAX) GO TO 40                                          !MMBI1180
      IF (AX.GE.XMIN) GO TO 5                                           !MMBI1190
      MMBSJ1 = 0.0D0                                                    !MMBI1200
      GO TO 9005                                                        !MMBI1210
    5 IF (AX.GT.XSMALL) GO TO 10                                        !MMBI1220
      MMBSJ1 = ARG/2.0D0                                                !MMBI1230
      GO TO 9005                                                        !MMBI1240
   10 IF (AX.GT.8.0D0) GO TO 30                                         !MMBI1250
      Y = AX*AX                                                         !MMBI1260
      IF (AX.GT.4.0D0) GO TO 20                                         !MMBI1270
C                                  XSMALL .LT. ABS(ARG) .LE. 4.0        !MMBI1280
      XNUM = P(6)*Y+P(5)                                                !MMBI1290
      DEN = Y+Q(5)                                                      !MMBI1300
      DO 15 I=1,4                                                       !MMBI1310
         XNUM = XNUM*Y+P(I)                                             !MMBI1320
         DEN = DEN*Y+Q(I)                                               !MMBI1330
   15 CONTINUE                                                          !MMBI1340
      Z = (AX-X01)-X02                                                  !MMBI1350
      MMBSJ1 = (XNUM / DEN) * ARG * Z * (AX + X01)                      !MMBI1360
      GO TO 9005                                                        !MMBI1370
C                                  4.0 .LT. ABS(ARG) .LE. 8.0           !MMBI1380
   20 XNUM = 0.0D0                                                      !MMBI1390
      DEN = 0.5D0                                                       !MMBI1400
      DO 25 I=1,6                                                       !MMBI1410
         XNUM = XNUM*Y+B(I)                                             !MMBI1420
         DEN = DEN*Y+D(I)                                               !MMBI1430
   25 CONTINUE                                                          !MMBI1440
      XNUM = XNUM*(AX-8.0D0)*(AX+8.0D0)+B(7)                            !MMBI1450
      XNUM = XNUM*(AX-4.0D0)*(AX+4.0D0)+B(8)                            !MMBI1460
      Z = (AX-X11)-X12                                                  !MMBI1470
      MMBSJ1 = (XNUM/DEN)*ARG*Z*(AX+X11)                                !MMBI1480
      GO TO 9005                                                        !MMBI1490
C                                  ABS(ARG) .GT. 8.0                    !MMBI1500
   30 XC = RTPI2/DSQRT(AX)                                              !MMBI1510
      IF (ARG.LT.0.0D0) XC = -XC                                        !MMBI1520
      Z = 8.0D0/AX                                                      !MMBI1530
      ZSQ = Z*Z                                                         !MMBI1540
C      V = ((AX/TWOPI1+XMAX1)-XMAX1)+0.375D0                            ! MMBI1550
      V = INT(AX/TWOPI1)+0.375D0                                        !MMBI1550
      V = (AX-V*TWOPI1)-V*TWOPI2                                        !MMBI1560
      XNUM = AP1(6)                                                     !MMBI1570
      DEN = 1.0D0                                                       !MMBI1580
      XNUM2 = AP2(6)                                                    !MMBI1590
      DEN2 = 1.0D0                                                      !MMBI1600
      DO 35 I=1,5                                                       !MMBI1610
         XNUM = XNUM*ZSQ+AP1(I)                                         !MMBI1620
         DEN = DEN*ZSQ+AQ1(I)                                           !MMBI1630
         XNUM2 = XNUM2*ZSQ+AP2(I)                                       !MMBI1640
         DEN2 = DEN2*ZSQ+AQ2(I)                                         !MMBI1650
   35 CONTINUE                                                          !MMBI1660
      MMBSJ1 = XC*((XNUM/DEN)*DCOS(V)-Z*(XNUM2/DEN2)*DSIN(V))           !MMBI1670
      GO TO 9005                                                        !MMBI1680
   40 MMBSJ1 = 0.0D0                                                    !MMBI1690
      IER = 129                                                         !MMBI1700
 9000 CONTINUE                                                          !MMBI1710
c     CALL UERTST (IER,6HMMBSJ1)                                        !MMBI1720
 9005 RETURN                                                            !MMBI1730
      END                                                               !MMBI1740
