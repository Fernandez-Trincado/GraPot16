! Program to compute test particles
! Last update: 2016, March
! Author: J. G. Fernandez-Trincado, A. C. Robin, O. Bienayme, E. Moreno, et al.   
! Axisymmetric Galactic model based in the Besancon Galaxy model (Robin et al. 2014) 
! Comments: Non-axisymmetric structures to modeling the triaxial bar is provided in a separate Fortran Subroutine. 
! Output: Potential "POTENTIAL", Force in R "KRPLANE", and Force in z "KZPLANE".
! Units: Potential = (km/s)^2 , Force = (km/s)^2 * (1/pc)

	SUBROUTINE ICPOTrz(RinputIC, zinputIC, KRPLANE, KZPLANE, POTENTIAL, POTENTIALZ) 
	IMPLICIT DOUBLE PRECISION (A-H,O-Z)
	EXTERNAL POT2015, RADIALFORCE, KZ2016
        INTEGER  OMP_GET_THREAD_NUM 
	INTEGER iag 
        COMMON/EXPGEN/XXL(15)
        COMMON/BORNINT/BM,BP,N1,N2,N,NN
        COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE ! WARNING: R is defined in the plane (x,y) and Z that is perpendicular
        COMMON/CORO/DCcore,DCdensity,CMcore,CMdensity,EXPOCO,EPSICO
        double precision kp1,km1,kp2,km2,kp3,km3, KRPLANE, KZPLANE, POTENTIAL, POTENTIAL2, KZPLANE2, POTENTIAL2Z, POTENTIALZ
        double precision Input_x, Input_y, Input_z, input_R, KZ2016, RinputIC, zinputIC
        COMMON/DISC/kp1,km1,kp2,km2,kp3,km3,rdis,rhole,EXC(15),iaghole
        COMMON/CIN/SIGW(15)
!        common/bar/Mbar, balpha,  axRpc, ayRpc, azRpc, ABRpc,HBRpc,anglebar
!        common/SHalo/SHahern,SHdhern,SHq,SHcore,SHSunDensity,kler,n_part1,xdat
        real*8 xdat(300)
        integer kler,kchoice,i,s,j
        integer n_part1
        character*80 default
        integer iaghole,KLE,klef,kleo,klep,klew,kleq,sub, ir 
        real*8 svs(15),dnat(15),xn,xe,pot(15),pot1(15),pot2(15)
        real*8 dens(15), PotRZ, ParaPotRZ(14)
        real*8 for(15),vite(15),Kz(15), Input_XC, Input_YC, Input_ZC
        character*80 string
        real*8 SHalpha,SHbeta,SHgamma
        INCLUDE 'GraPotConfigurationfile.dat' ! Constants from the Besancon Galaxy model 
        SHSunDensity=dvs(9)
        DO i=1,15
		exc(i)=dsqrt(1.d0-e(i)*e(i))
        ENDDO

	Rgal = RinputIC ! Galactocentric coordinates, Rgal = SQRT( X^2 + Y^2 ) in units of pc 
	Zgal = zinputIC ! Height above the plane in units of pc

! Computing the Force___________________________________________________________________________________________________________________________
! For Kr, only z=0 applies
! Integration paramters. 
! NN_MAX: 10000  N_MAX : 5000                                             
! BP AND NN  EXPONENTIAL DISK; N ELLIPSOIDE                                
        NN        = 500.
        BP        = 500.
        N         = 500.
        z         = 0.
        KRPLANE   = 0.
        KZPLANE   = 0.
        POTENTIAL = 0.
        POTENTIALZ = 0.

        DO i = 14, 14
            IAG = i
            CALL RADIALFORCE(IAG,Rgal,Zgal,FOR(IAG))        ! Force in R in units (km/s)^2 (1/pc)
	    KRPLANE     = FOR(IAG) + KRPLANE

            KZPLANE2    = KZ2016(IAG,Rgal,Zgal)             ! Force in z in units (km/s)^2 (1/pc)
            KZPLANE     = KZPLANE2 + KZPLANE

            POTENTIAL2  = POT2015(IAG,Rgal,Zgal,xgal,ygal)  ! Potential in (km/s)^2
            POTENTIAL   = POTENTIAL2 + POTENTIAL
	
	    POTENTIAL2Z = POT2015(IAG,Rgal,Zgal+1.,xgal,ygal) 
            POTENTIALZ  = POTENTIALZ + (POTENTIAL2 - POTENTIAL2Z)


        ENDDO


	IF (Rgal.EQ.0.AND.Zgal.EQ.0) THEN

		KRPLANE = 0
		KZPLANE = 0
		POTENTIAL = 0
	ENDIF

	RETURN
        END
