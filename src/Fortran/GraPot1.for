!-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------    -------------
! Program: Input coordinates (Rgal, zgal)
! Last update: 2016, March
! Author: J. G. Fernandez-Trincado, A. C. Robin, O. Bienayme, E. Moreno, A. Perez-Villegas, B. Pichardo, O. Valenzuela, et al.   
! Inputs:  RinputIC = Rgal [pc], and zinputIC = Zgal [pc] 
! Input Units: pc
! Output: Potential POTENTIAL, Force in R KRPLANE, and Force in z KZPLANE.
! Output Units: Potential = (km/s)^2 , Force = (km/s)^2 * (1/pc)
! To Compile: gfortran -fopenmp -fbounds-check -fcheck-array-temporaries -Wsurprising -ffixed-line-length-none -o GraPot16 GraPot1.for GraPot2.for GraPot3.for GraPot4.for GraPot5.for GraPot6.for
! To run: ./BGM
!-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------    -------------

        PROGRAM INPUTCOORDINATES
	IMPLICIT NONE
	EXTERNAL ICPOTrz
	DOUBLE PRECISION RinputIC, zinputIC, KRPLANE, KZPLANE, POTENTIAL, POTENTIALZ
	DOUBLE PRECISION X, Y, Ri, Fxnew, Fynew, l

	!X = 707.1067811865476 !6011.4021940432121
	!Y = 707.1067811865476 !9203.0566453122628
	!Ri = 3500. !DSQRT(X*X + Y*Y)
	X = 1000
	Y = 1000
	l = 1000
	Ri = DSQRT(X*X + Y*Y)
	
	!DO l = 1, 6000, 50
	RinputIC = Ri                        ! change	
	zinputIC = l                         ! change

        CALL ICPOTrz(RinputIC, zinputIC, KRPLANE, KZPLANE, POTENTIAL, POTENTIALZ)

	Fxnew = KRPLANE*(X/RinputIC)
	Fynew = KRPLANE*(Y/RinputIC)

	write(*,*) "R",RinputIC,"X:",X, "Y:",Y, "Z", l, "Fx:",Fxnew, "Fy:",Fynew, "Fz:", KZPLANE

	!WRITE(*,*) RinputIC, zinputIC, POTENTIALZ, KZPLANE
	!ENDDO

	!write(*,*) "Fz from Olivier: ",POTENTIALZ, "Fz:",KZPLANE
!	write(*,*) "Fx:",Fxnew, "Fy:",Fynew, "Fx^2 + Fy^2 = KRPLANE^2 :",SQRT(Fxnew*Fxnew + Fynew*Fynew), "KRPLANE:",KRPLANE,"Vc:", DSQRT(KRPLANE*RinputIC)
!	write(*,*) "R:",RinputIC, "z:",zinputIC, "FR:",KRPLANE, "Fz:",KZPLANE, "Potential:",POTENTIAL, "Vc:", DSQRT(KRPLANE*Ri)

                      
	END 
