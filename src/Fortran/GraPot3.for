! MAIN PROGRAM 
! Potential and Field Forces based in the Besancon Galaxy Model
! Besancon references: 
! [1] Bienayme et al. (1987)
! [2] Robin et al. (2003)
! [3] Robin et al. (2012)
! [4] Robin et al. (2014)
! [5] Einasto (1979) 
!
! Milky Way Model: 
! [1] Seven components of the Thin disk: Modified version of the Einasto spheroid with cylindrical hole in the center   
! [2] Two components of the Thick disk:
!       [2.1] Version A (This work): This is a density that decomposes vertically into a parabolic shape at short distances from the plane followed by an exponential (Robin et al. 2014)
!       [2.2] Version B (In preparation for Potential): Simple hyperbolic secant squared (Robin et al. 2014) 
! [3] Interstellar Matter (2003)
! [4] Pseudo-Isothermal Dark Matter Halo (Begeman et al. 1991 and Robin et al. 2003) 
! [5] Central Bulge or Central Mass, Spherically symmetric potential of the form (Miyamoto and Nagai 1975, Allen & Santillan 1991, Robin et al. 2003)   
! [6] Galactic Bar (Not this work)
!
! Last Update: 2016, March 03
!
!----------------------------------------------------                           
! CALCUL DE LA FORCE EN Z= 0 ET R DE LA COMPSANTE IAG                           
!----------------------------------------------------                           
      SUBROUTINE RADIALFORCE(IAG,Rgal,Zgal,FOR)                                                 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      COMMON/EXPGEN/XXL(15)                                                     
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      double precision kp1,km1,kp2,km2,kp3,km3,kflare
      COMMON/DISC/kp1,km1,kp2,km2,kp3,km3,rdis,rhole,EXC(15),iaghole
      COMMON/BORNINT/BM,BP,N1,N2,N,NN
      COMMON/CORO/DCcore,DCdensity,CMcore,CMdensity,EXPOCO,EPSICO
      DOUBLE PRECISION Mbar, balpha, bbeta, bgamma,anglebar,Forcedh2016
      DOUBLE PRECISION axRpc, ayRpc, azRpc, ABRpc, HBRpc
      common/bar/Mbar, balpha,  axRpc, ayRpc, azRpc, ABRpc, HBRpc, anglebar
      common/SHalo/SHahern,SHdhern,SHq,SHcore,SHSunDensity,kler,n_part1,xdat
      REAL*8 SHahern,SHdhern,SHq,SHcore,SHSunDensity,FRSH,FZSH,POTSH,xdat(300)
      integer kler,n_part1,kplus
      
      real*8 for,forpos,forneg,ell

      kler=1  !form dark halo
      
      IF(IAG.LE.7) THEN !thin disc


 1       IF(KLE.EQ.1) THEN
            CALL FEXP(IAG,Rgal,Zgal,FOR)
            RETURN
         ELSEIF(KLE.NE.1) THEN
            call fell(iag,rgal,zgal,forold)  !for test: old value
            ell=exc(iag)
            call FELLDISC(1,IAG,ELL,RGAL,ZGAL,FORPOS) ! positive part
            IF(exc(iag).EQ.0.) THEN
               WRITE(6,*) 'wrong exc ', iag, exc(iag),e(iag)
               STOP
            ENDIF
            IF(IAG.EQ.1) THEN
               ell=exc(iag)*kp1/km1
            ELSE
               ell=exc(iag)*kp2/km2
            ENDIF

         CALL FELLDISC(-1,IAG,ELL,Rgal,Zgal,FORNEG) ! negative part

           FOR=FORPOS-FORNEG

         RETURN                                                                    
         ENDIF

      ELSEIF(IAG.EQ.8.OR.iag.EQ.11) THEN
 8    CALL Smith15(IAG,Rgal,Zgal,PoTSm15, FRTSm15, FZTSm15) ! exp(R/Rd)*sech^2(z/z0)
      FOR  = FRTSm15   
      RETURN

      ELSEIF(iag.EQ.9) THEN     !stellar halo
 9    CALL SHAprox2016(Rgal,Zgal,PhiSHAprox, FRSHAprox, FzSHAprox) 
      FOR = FRSHAprox
      RETURN

      ELSEIF(IAG.EQ.10) THEN    !boxybar  other units
 10      FOR  = 0.D0
         return

      ELSEIF(IAG.EQ.12) THEN 
 12   CALL  NewCM(Rgal,Zgal,POTTOTALcm,FRNCM16, FZNCM16)
      FOR = FRNCM16
      RETURN

      ELSEIF(IAG.EQ.13) THEN
 13   CALL Smith15(13,Rgal,Zgal,PoTSm15, FRTSm15, FZTSm15)  ! exp(-R/Rd)*exp(-|z|/zo)
      FOR = FRTSm15
      RETURN

      ELSEIF(IAG.EQ.14) THEN
 14   CALL  DMCbgm(Rgal,Zgal,phidh2016,FRbgmdm,FZbgmdm) 
      FOR = FRbgmdm
      RETURN

      ELSEIF(IAG.EQ.15) THEN            
 15   IF(KLE.EQ.2.OR.KLE.EQ.4) GO TO 99                                         
                                                  
      IF(DVS(15).LT.1.D-6) THEN                                                 
        FOR=0.D0                                                                
      ELSE                                                                      
        FOR=0.D0                                                        
      END IF
      END IF
      
 99   RETURN                                                                    
      END                                                                       

! ---> 2016: Compute the potential at point (rgal,zgal) ******************************************************************************************************
      FUNCTION KZ2016(IAG,Rgal,Zgal)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/EXPGEN/XXL(15)
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      double precision kp1,km1,kp2,km2,kp3,km3
      COMMON/DISC/kp1,km1,kp2,km2,kp3,km3,rdis,rhole,EXC(15),iaghole
      COMMON/BORNINT/BM,BP,N1,N2,N,NN
      COMMON/CORO/DCcore,DCdensity,CMcore,CMdensity,EXPOCO,EPSICO
      DOUBLE PRECISION Mbar, balpha, bbeta, bgamma,anglebar, KZ2016T, KZ2016P1, KZ2016
      DOUBLE PRECISION axRpc, ayRpc, azRpc, ABRpc, HBRpc, phidh2016
      common/bar/Mbar, balpha,  axRpc, ayRpc, azRpc, ABRpc, HBRpc, anglebar
      common/SHalo/SHahern,SHdhern,SHq,SHcore,SHSunDensity,kler,n_part1,xdat
      REAL*8 SHahern,SHdhern,SHq,SHcore,SHSunDensity,FRSH,FZSH,POTSH,xdat(300)
      integer kler,n_part1,kplus
      REAL*8 PHI(15)
      real*8 input_x,input_y,input_z,input_r,ell,phipos,phineg

! ---> 2016: THIN DISK **************************************************************************************************************************************   
      IF(iag.le.7) THEN
      ell=exc(iag)
      call POSPHDISC(1,IAG,ELL,Rgal,Zgal,PHIPOS) ! positive part
      IF(iag.EQ.1) THEN
         ell=exc(iag)*kp1/km1
      ELSE
         ell=exc(iag)*kp2/km2
      ENDIF
      CALL POSPHDISC(-1,IAG,ELL,Rgal,Zgal,PHINEG) ! negative part
      KZ2016T = PHIPOS-PHINEG

      ell=exc(iag)
      call POSPHDISC(1,IAG,ELL,Rgal,Zgal+1,PHIPOS) ! positive part  
      IF(iag.EQ.1) THEN
          ell=exc(iag)*kp1/km1
      ELSE
          ell=exc(iag)*kp2/km2
      ENDIF
      CALL POSPHDISC(-1,IAG,ELL,Rgal,Zgal,PHINEG) ! negative part
      KZ2016P1 = PHIPOS-PHINEG
      KZ2016   = KZ2016T - KZ2016P1
      RETURN
! ---> 2016: THICK DISK **************************************************************************************************************************************  
      ELSEIF(iag.EQ.8.OR.iag.EQ.11) THEN
 8    CALL Smith15(IAG,Rgal,Zgal,PoTSm15, FRTSm15, FZTSm15) ! exp(-R/Rd)*sech^2(-|z|/z0)
      KZ2016 = FZTSm15
      RETURN
! ---> 2016: STELLAR HALO ************************************************************************************************************************************     
      ELSEIF(iag.EQ.9) THEN
 9    CALL SHAprox2016(Rgal,Zgal,PhiSHAprox, FRSHAprox, FzSHAprox)
      KZ2016 = FzSHAprox
      RETURN
! ---> 2016: GALACTIC BAR, Not used in the BGM only for TEST PARTICLE SIMULATIONS  *************************************************************************** 
      ELSEIF(iag.EQ.10) THEN
 10   KZ2016 = 0.        ! Not used in the BGM only for TEST PARTICLE SIMULATIONS
      RETURN
! ---> 2016: CENTRAL MASS ************************************************************************************************************************************ 
      ELSEIF(iag.EQ.12) THEN
 12   CALL  NewCM(Rgal,Zgal,POTTOTALcm,FRNCM16, FZNCM16)
      KZ2016  = FZNCM16
      RETURN
! ---> 2016: INTESTELLAR MATTER ****************************************************************************************************************************** 
      ELSEIF(iag.EQ.13) THEN
 13   CALL Smith15(13,Rgal,Zgal,PoTSm15, FRTSm15, FZTSm15)  ! exp(-R/Rd)*exp(-|z|/zo)   
      KZ2016  = FZTSm15
      RETURN
      ELSEIF(iag.EQ.14) THEN
 14   CALL DMCbgm(Rgal,Zgal,phidh2016,FRbgmdm,FZbgmdm)
      KZ2016  = FZbgmdm
      RETURN
! --->  2016: Not used - REMOVE ******************************************************************************************************************************
      ELSEIF(iag.EQ.15) THEN 
 15   KZ2016 = 0. 
      ENDIF
 99   RETURN
      END
!---------------------------------------------------------------------------------------------------------------------------------------------------

                                                                              
! ---> 2016: Compute the potential at point (rgal,zgal) ******************************************************************************************************
      FUNCTION POT2015(IAG,Rgal,Zgal)                      
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/EXPGEN/XXL(15)   
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      double precision kp1,km1,kp2,km2,kp3,km3
      COMMON/DISC/kp1,km1,kp2,km2,kp3,km3,rdis,rhole,EXC(15),iaghole
      COMMON/BORNINT/BM,BP,N1,N2,N,NN
      COMMON/CORO/DCcore,DCdensity,CMcore,CMdensity,EXPOCO,EPSICO
      DOUBLE PRECISION Mbar, balpha, bbeta, bgamma,anglebar
      DOUBLE PRECISION axRpc, ayRpc, azRpc, ABRpc, HBRpc, phidh2016
      common/bar/Mbar, balpha,  axRpc, ayRpc, azRpc, ABRpc, HBRpc, anglebar
      common/SHalo/SHahern,SHdhern,SHq,SHcore,SHSunDensity,kler,n_part1,xdat
      REAL*8 SHahern,SHdhern,SHq,SHcore,SHSunDensity,FRSH,FZSH,POTSH,xdat(300)
      integer kler,n_part1,kplus
      REAL*8 PHI(15)
      real*8 input_x,input_y,input_z,input_r,ell,phipos,phineg

! ---> 2016: THIN DISK **************************************************************************************************************************************   
      IF(iag.le.7) THEN
      ell=exc(iag)
      call POSPHDISC(1,IAG,ELL,Rgal,Zgal,PHIPOS) ! positive part
      IF(iag.EQ.1) THEN
         ell=exc(iag)*kp1/km1
      ELSE
         ell=exc(iag)*kp2/km2
      ENDIF
      CALL POSPHDISC(-1,IAG,ELL,Rgal,Zgal,PHINEG) ! negative part
      POT2015 = PHIPOS-PHINEG
      RETURN                
! ---> 2016: THICK DISK **************************************************************************************************************************************  
      ELSEIF(iag.EQ.8.OR.iag.EQ.11) THEN
 8    CALL Smith15(IAG,Rgal,Zgal,PoTSm15, FRTSm15, FZTSm15) ! exp(-R/Rd)*sech^2(-|z|/z0)
      POT2015 = PoTSm15
      RETURN 
! ---> 2016: STELLAR HALO ************************************************************************************************************************************     
      ELSEIF(iag.EQ.9) THEN                     
 9    CALL SHAprox2016(Rgal,Zgal,PhiSHAprox, FRSHAprox, FzSHAprox)
      POT2015 = PhiSHAprox
      RETURN
! ---> 2016: GALACTIC BAR, Not used in the BGM only for TEST PARTICLE SIMULATIONS  *************************************************************************** 
      ELSEIF(iag.EQ.10) THEN
 10   POT2015 = 0.        ! Not used in the BGM only for TEST PARTICLE SIMULATIONS
      RETURN
! ---> 2016: CENTRAL MASS ************************************************************************************************************************************ 
      ELSEIF(iag.EQ.12) THEN
 12   CALL  NewCM(Rgal,Zgal,POTTOTALcm,FRNCM16, FZNCM16)   
      POT2015  = POTTOTALcm
      RETURN                
! ---> 2016: INTESTELLAR MATTER ****************************************************************************************************************************** 
      ELSEIF(iag.EQ.13) THEN
 13   CALL Smith15(13,Rgal,Zgal,PoTSm15, FRTSm15, FZTSm15)  ! exp(-R/Rd)*exp(-|z|/zo)   
      POT2015  = PoTSm15 
      RETURN
      ELSEIF(iag.EQ.14) THEN
 14   CALL DMCbgm(Rgal,Zgal,phidh2016,FRbgmdm,FZbgmdm)
      POT2015  = phidh2016
      RETURN
! --->  2016: Not used - REMOVE ******************************************************************************************************************************
      ELSEIF(iag.EQ.15) THEN ! missing mass disc (deprecated)
 15   POT2015 = 0. ! Not used
      ENDIF
 99   RETURN                  
      END                    
!-------------------------------------------------------------------------------------------------------------------------------------------------------
!
!
!
!
      
      SUBROUTINE POSPH(IAG,Rgal,Zgal,PHI)                                                 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      EXTERNAL RHODISC2015c,RHOHALOH
      REAL*8 RHODISC2015C,RHOHALOH
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      double precision kp1,km1,kp2,km2,kp3,km3
      COMMON/DISC/kp1,km1,kp2,km2,kp3,km3,rdis,rhole,EXC(15),iaghole
      COMMON/BORNINT/BM,BP,N1,N2,N,NN
      COMMON/CORO/DCcore,DCdensity,CMcore,CMdensity,EXPOCO,EPSICO                          
      DIMENSION PHI(15),PHITE(15)                                               
      DIMENSION A1(10000),A2(10000),SOL1(10000),SOL2(10000)                     
      DATA PI/3.14159265358D0/,G/4.29569D-03/   
      
      
!  UNITE DE G: PC * (KM/S)**2 /M0                                               
!       ..UNITE DE POTENTIEL: (KM/S)**2                                         
!                                      

!      rchalo=2500. d0


!  BORNES D'INTEGRATION:                                                        
      B1I=0.D0                                                                  
      B1S=DASIN(E(IAG))                                                        
      B2I = (Rgal**2+Zgal**2/(1-E(IAG)**2))**0.5                                      
      B2S=B2I+2.D6                                                              
      DJ1=(B1S-B1I)/(N1-1)                                                      
      DJ2=(B2S-B2I)/(N2-1)  
      !write(69,*) 'debut POSPH ',iag, e(iag), rgal, zgal, kp1, kp2

      DO 2 J=1,N1                                                               
      BETA1=DJ1*(J-1) + B1I                                                     
      DEV=Rgal**2*DSIN(BETA1)*DCOS(BETA1)+Zgal**2*DTAN(BETA1)/(DCOS(BETA1)**2)        
      IF(E(IAG).GT..1D-05) then
         DEV=DEV/(E(IAG)**2.)     
      else
         DEV=DEV*1.D+12   
      endif
      ALPHA=(Rgal*DSIN(BETA1))**2 + (Zgal*DTAN(BETA1))**2                             
      IF(E(IAG).GT..1D-05) then
         ALPHA=(ALPHA**0.5)/E(IAG) 
      else
         ALPHA=ALPHA**0.5*1.D+06 
      endif
      IF(IAG.LE.7) then
         !write(66,*) 'alpha=',alpha,rgal,zgal,e(iag)
         den=RHODISC2015C(IAG,Rgal,Zgal)
         !write(66,*) 1,iag,alpha,rgal,zgal,kp1,km1,kp2,km2,e(iag),den
         A1(J)= den * DEV * BETA1  
!      elseif(IAG.EQ.9.and.kle.ne.5) then     !stellar halo
!         A1(J)=RHOHALO(IAG,ALPHA) * DEV * BETA1  
      elseif(IAG.EQ.9.and.kle.eq.5) then     !stellar halo Hernquist 
         A1(J)=RHOHALOh(IAG,ALPHA,rchalo) * DEV * BETA1  
      elseif(IAG.EQ.14) then    ! spherical corona
         A1(J)=DCdensity/(1+(ALPHA/DCcore)**EXPOCO)  * DEV * BETA1
      elseif(iag.eq.11) then
!         write(6,*) 'POSPH iag=11 deprecated, should be computed as the thick disc, not here'
         stop

      else
!         write(6,*) 'bad try POSPH, population not ellipsoid ',iag
         stop
      endif
 2    CONTINUE                                                                  
C                                                                               
C   INTEGRATION - METHODE DE SIMPSON                                            
      CALL DQSF(DJ1,A1,SOL1,N1)                                                 
C                                                                               
      DO 3 J=1,N2                                                               
      ALPHA=DJ2*(J-1) + B2I    
      IF(IAG.LE.7) then
         den=RHODISC2015C(IAG,Rgal,Zgal)
         !write(66,*) 2,iag,alpha,rgal,zgal,kp2,km2,e(iag),den
         A2(J) = den*ALPHA   
!      elseif(IAG.EQ.9.and.kle.ne.5) then !halo power law
!         A2(J) = RHOHALO(IAG,ALPHA)*ALPHA 
      elseif(IAG.EQ.9.and.kle.eq.5) then !halo Hernquist
         A2(J) = RHOHALOh(IAG,ALPHA,rchalo)*ALPHA 
      elseif(IAG.EQ.14) then    ! spherical corona
         A2(J)=DCdensity/(1+(ALPHA/DCcore)**EXPOCO) *ALPHA
      elseif(iag.eq.11) then
!         write(6,*) 'POSPH iag=11 deprecated, should be computed as the thick disc, not here'
         stop
      else
!         write(6,*) 'bad try POSPH, population not ellipsoid ',iag
         stop
      endif

                                           
 3    CONTINUE                                                                  
C                                                                               
C   INTEGRATION - METHODE DE SIMPSON                                            
      CALL DQSF(DJ2,A2,SOL2,N2)                                                 
C                                                                               
C  POTENTIEL                                                                    
C      CALL TEST1(IAG,PHITE,A1T,A2T)                                             
!      WRITE(6,*) 'IAG=',IAG,N1,N2
!      WRITE(6,*) 'SOL1=',SOL1(N1),SOL2(N2)                               
 21   FORMAT(2X,'IAG=',I3,'  A1 NUMERIC:',E20.9,'  A1 CALCULE:',E20.9           
     &,/,9X,'  A2 NUMERIC:',E20.9,'  A2 CALCULE:',E20.9)                        
      PHI(IAG)=SOL1(N1) + DASIN(E(IAG)) * SOL2(N2)                             
      PHI(IAG)=PHI(IAG)*4*PI*G*(1-E(IAG)**2)**(0.5)                             
      PHI(IAG)=PHI(IAG)/E(IAG)                                                  
      !WRITE(6,*) 'SUB POSPH',IAG,PHI(IAG)                                           
      RETURN                                                                    
      END                                                                       

!     ----------------
!     thin disc formula with cylindrical hole in the center
!     ------
      
      SUBROUTINE POSPHDISC(kplus,IAG,ELL,Rgal,Zgal,Phipartdisc)                                                 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      EXTERNAL RHODISC2015C,RHOHALOH
      REAL*8 RHODISC2015C,RHOHALOH
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      double precision kp1,km1,kp2,km2,kp3,km3
      COMMON/DISC/kp1,km1,kp2,km2,kp3,km3,rdis,rhole,EXC(15),iaghole
      COMMON/BORNINT/BM,BP,N1,N2,N,NN
      COMMON/CORO/DCcore,DCdensity,CMcore,CMdensity,EXPOCO,EPSICO                          
      real*8 PHIpartdisc,PHITE(15)                                               
      DIMENSION A1(10000),A2(10000),SOL1(10000),SOL2(10000)                     
      real*8 PI/3.14159265358D0/,G/4.29569D-03/
      integer kplus
      real*8 a01, a02, d01, d02,ellell
      
!  UNITE DE G: PC * (KM/S)**2 /M0                                               
!       ..UNITE DE POTENTIEL: (KM/S)**2                                         
!                                      

!     rchalo=2500. d0
!     ell is corrected value of the excentricity : ell=exc if kplus=1 and ell=exc*kp2/lm2 if kplus=-1

      a01=(R0*R0+zed0*zed0/(exc(iag)*exc(iag)))/(kp1*kp1)
      a01m=(R0*R0+zed0*zed0/(exc(iag)*exc(iag)))/(km1*km1) !il faut utiliser ell ou exc ? ell est modifie par les echelle de longueur, exc pas
! lancienne formule (Einasto) et son potentiel utilisait exc, donc pour tester qu on retrouve il faut prendre exc. Mais comme zed0 est proche de zero, par rappor r0 a ne donne pas de diffrence.
      d01=DEXP(-a01)-dexp(-a01m)

      a02=(R0*R0+zed0*zed0/(exc(iag)*exc(iag)))/(kp2*kp2)
      a02m=(R0*R0+zed0*zed0/(exc(iag)*exc(iag)))/(km2*km2)
      d02=DEXP(0.5D0-dsqrt(0.25d0+a02))-DEXP(0.5D0-dsqrt(0.25d0+a02m))
c      WRITE(*,*) "R0, z0", R0, zed0

        ellell=dsqrt(1.d0-ell*ell) !ellipticity new, replace e(iag)

!  BORNES D INTEGRATION:
      B1I=0.D0                                                                  
      B1S=DASIN(ELLELL)
      B2I = (Rgal**2+(Zgal/ell)**2)**0.5
      B2S=B2I+2.D6                                                              
      DJ1=(B1S-B1I)/(N1-1)                                                      
      DJ2=(B2S-B2I)/(N2-1)  
      !write(69,*) "debut POSPH ",iag, ell, rgal, zgal, kp1, kp2
                                                                               
      DO 2 J=1,N1                                                               
      BETA1=DJ1*(J-1) + B1I                                                     
      DEV=Rgal**2*DSIN(BETA1)*DCOS(BETA1)+Zgal**2*DTAN(BETA1)/(DCOS(BETA1)**2)        
      IF(ELLELL.GT..1D-05) then
         DEV=DEV/(ELLELL**2.)
      else
         DEV=DEV*1.D+12   
      endif
      ALPHA=(Rgal*DSIN(BETA1))**2 + (Zgal*DTAN(BETA1))**2                             
      IF(ELLELL.GT..1D-05) then
         ALPHA=(ALPHA**0.5)/ELLELL
      else
         ALPHA=ALPHA**0.5*1.D+06 
      endif
      if(iag.eq.1.and.kplus.eq.1) then
         den=DEXP(-alpha*alpha/(KP1*KP1))*dvs(iag)/d01
      elseif(iag.eq.1.and.kplus.eq.-1) then
         den=DEXP(-alpha*alpha/(KM1*KM1))*dvs(iag)/d01
      elseif(iag.le.7.and.kplus.eq.1) then
         den=DEXP(0.5D0-dsqrt(0.25d0+alpha*alpha/(kp2*kp2)))*dvs(iag)/d02
      elseif(iag.le.7.and.kplus.eq.-1) then
         den=DEXP(0.5D0-dsqrt(0.25d0+alpha*alpha/(km2*km2)))*dvs(iag)/d02
      endif
      !write(57,*) iag,rgal,zgal,kplus,den,d01,d02

      A1(J)= den * DEV * BETA1  

 2    CONTINUE                                                                  
C                                                                               
C   INTEGRATION - METHODE DE SIMPSON                                            
      CALL DQSF(DJ1,A1,SOL1,N1)                                                 
C                                                                               
      DO 3 J=1,N2                                                               
         ALPHA=DJ2*(J-1) + B2I
         if(iag.eq.1.and.kplus.eq.1) then
            den=DEXP(-alpha*alpha/(KP1*KP1))*dvs(iag)/d01
         elseif(iag.eq.1.and.kplus.eq.-1) then
            den=DEXP(-alpha*alpha/(KM1*KM1))*dvs(iag)/d01
         elseif(iag.gt.1.and.kplus.eq.1) then
            den=DEXP(0.5D0-dsqrt(0.25d0+alpha*alpha/(kp2*kp2)))*dvs(iag)/d02
         elseif(iag.gt.1.and.kplus.eq.-1) then
            den=DEXP(0.5D0-dsqrt(0.25d0+alpha*alpha/(km2*km2)))*dvs(iag)/d02
         endif
         !write(57,*) iag,rgal,zgal,kplus,den,d01,d02

         A2(J) = den*ALPHA   
                                           
 3    CONTINUE                                                                  
C                                                                               
C   INTEGRATION - METHODE DE SIMPSON                                            
      CALL DQSF(DJ2,A2,SOL2,N2)                                                 
C                                                                               
C  POTENTIEL                                                                    
C      CALL TEST1(IAG,PHITE,A1T,A2T)                                             
!      WRITE(6,*) "IAG=",IAG,N1,N2
!      WRITE(6,*) "SOL1=",SOL1(N1),SOL2(N2)

 21   FORMAT(2X,'IAG=',I3,'  A1 NUMERIC:',E20.9,'  A1 CALCULE:',E20.9           
     &,/,9X,'  A2 NUMERIC:',E20.9,'  A2 CALCULE:',E20.9)                        

      PHIpartdisc=SOL1(N1) + DASIN(ELLELL) * SOL2(N2)
      PHIpartdisc=PHIpartdisc*4.*PI*G*ELL
      PHIpartdisc=PHIpartdisc/ELLELL
        !WRITE(85,*) 'SUB POSPHDISC',IAG,rgal,zgal,kplus,PHIpartdisc
      
      RETURN                                                                    
      END                                                                       

C______________________
C                                                                               
C   CALCUL DU POTENTIEL POUR LES LOIS EXPONENTIELLES                            
      SUBROUTINE POEXP(IAG,Rgal,Zgal,PHI)                                                 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      DOUBLE PRECISION MMBSJ0                                                   
      EXTERNAL ZII                                                              
      EXTERNAL MMBSJ0                                                           
      DIMENSION PHI(15)                                                         
      DIMENSION PFUN(10000), APFUN(10000),SOL(10000)                            
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      COMMON/BORNINT/BM,BP,N1,N2,N,NN
      DATA PI/3.14159265358d0/, G/4.29569D-03/                                    
      ALP=1/HL(IAG)                                                             
      BET=1/HH(IAG)                                                             
C                                                                               
C  CALCUL DU POTENTIEL PHI:                                                     
C    =SOMM(J0(K*R)*I(Z,K)*RHOC*ALP/(ALP**2+K**2)**1.5) *DK                      
C                                                                               
C  CHANGEMENT DE VARIABLES:                                                     
C     QR=K*R                                                                    
C     QA=K/ALP=QR/(ALP*R)                                                       
C                                                                               
      QTEST=ALP*Rgal                                                               
      IF(QTEST.LT..0001) WRITE(6,666) QTEST,Rgal,iag                                    
 666  FORMAT(' QTEST TOO CLOSE to 0',E20.9,' Rgal ',f10.2, 'Pop ',i3)                               
      IF(QTEST.LT..0001) RETURN                                                
C                                                                               
C   BORNES D'INTEGRATION                                                        
      DK=(BP-BM)/FLOAT(N-1)                                                     
      DO 100 I = 1, N                                                           
      QR = (I-1) * DK                                                           
      BJ=MMBSJ0(QR,IER)                                                         
 644  FORMAT(2X,D10.4,2X,I4,2X,D20.9)                                           
      IF(IER.NE.0) WRITE(6,667) IER , I                                         
 667  FORMAT(2X,2I4)                                                            
      POM=1/((1+(QR/(ALP*Rgal))**2)**(1.5))                                        
      APFUN(I)=BJ*ZII(QR,Rgal,Zgal,IAG)*POM                                               
 100  CONTINUE                                                                  
      DO 200 I=1,N                                                              
      PFUN(I)=APFUN(N-I+1)                                                      
 200  CONTINUE                                                                  
C                                                                               
C   INTEGRATION DE PFUN - SIMPSON'S RULE: QSF                                   
      CALL DQSF(DK,PFUN,SOL,N)                                                  
C                                                                               
C   VALEUR DU POTENTIEL                                                         
      ROC=DVS(IAG)*DEXP(R0/HL(IAG))                                             
      PHI(IAG)=SOL(N)*4.*PI*G*ROC/(ALP*ALP*Rgal)                                   
      RETURN                                                                    
      END                                                                       
C                                                                               
C  CALCUL DE LA FONCTION ZII                                                    
C ATTENTION CETTE QUANTITE EST DIVISEE PAR DEUX SI ON LA        
C COMPARE AUX DEVELOPPMENT ANALYTIQUE  ( LE DEUX SE RETROUVE DANS POEXP         
C OU POEXP2 :   ON A UN 4 PI ROC  AU LIEU DE 2 PI ROC  )                         
      FUNCTION ZII(QR,Rgal,Zgal,IAG)                                                      
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      BET=1/HH(IAG)                                                             
      ALP=1/HL(IAG)                                                             
      IF(QR.EQ.0.) THEN                                                         
        ZII=0.                                                                  
        GO TO 99                                                                
      END IF                                                                    
      IF(DABS(QR-BET*Rgal).LT.1.D-03) THEN                                         
        ZII=Rgal*DEXP(-QR*Zgal/Rgal)/(2*QR)                                              
      ELSE                                                                      
        IF(BET*DABS(Zgal).LE.100.) THEN                                            
           ZII1=(QR/Rgal)*DEXP(-BET*DABS(Zgal))                                       
        ELSE                                                                    
           ZII1=0.                                                              
        ENDIF                                                                   
        IF(QR*DABS(Zgal)/Rgal.LE.100.) THEN                                           
           ZII2=BET*DEXP(-QR*DABS(Zgal)/Rgal)                                         
        ELSE                                                                    
           ZII2=0.                                                              
        ENDIF                                                                   
        ZII=ZII1-ZII2                                                           
        ZII=ZII/((QR/Rgal)**2-BET**2)                                              
      ENDIF                                                                     
 99   RETURN                                                                    
      END                                                                       
                                                                               
      ! CALCUL DU POTENTIEL POUR LES LOIS EXPONENTIELLES MODIFIEES 27/02/86         
      ! RHO(Z) = EXP(-BETA*Z)   SI Z > XL                                           
      !        = A*Z*Z + B      SI Z < XL                                           
      SUBROUTINE POEXP2(IAG,Rgal,Zgal,PHI)                                                
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      DOUBLE PRECISION MMBSJ0                                                   
      EXTERNAL ZII                                                              
      EXTERNAL ZII2                                                             
      EXTERNAL MMBSJ0                                                           
      DIMENSION PHI(15)                                                         
      DIMENSION PFUN(10000), APFUN(10000),SOL(10000)                            
                                                                               
      ! VALEUR DE XL                                                                  
      COMMON/EXPGEN/XXL(15)                                                     
                                                                               
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      COMMON/BORNINT/BM,BP,N1,N2,N,NN
      DATA PI/3.14159265358D0/, G/4.29569D-03/                                    
      ALP=1/HL(IAG)                                                             
      BET=1/HH(IAG)                                                             
                                                                               
      ! CALCUL DU POTENTIEL PHI:                                                     
      !   =SOMM(J0(K*R)*I(Z,K)*RHOC*ALP/(ALP**2+K**2)**1.5) *DK                      
      !                                                                              
      ! CHANGEMENT DE VARIABLES:                                                     
      !    QR=K*Rgal                                                                    
      !    QA=K/ALP=QR/(ALP*Rgal)                                                       
      !                                                                              
      QTEST=ALP*Rgal                                                               
       IF(QTEST.LT..0001) WRITE(6,666) QTEST                                    
666    FORMAT(' QTEST TROP PROCHE DE ZERO',E20.9)                               
       IF(QTEST.LT..0001) RETURN                                                
                                                                               
      ! BORNES D'INTEGRATION                                                        
      DK=(BP-BM)/FLOAT(N-1)                                                     
      DO 100 I = 1, N                                                           
      QR = (I-1) * DK                                                           
      BJ=MMBSJ0(QR,IER)                                                         
 644  FORMAT(2X,D10.4,2X,I4,2X,D20.9)                                           
      IF(IER.NE.0) WRITE(6,667) IER , I                                         
 667  FORMAT(2X,2I4)                                                            
      POM=1/((1+(QR/(ALP*Rgal))**2)**(1.5))                                        
      ! APFUN(I)=BJ*ZII(QR,IAG)*POM                                               
                                                                               
      APFUN(I)=BJ*ZII2(QR,Rgal,Zgal,IAG)*POM                                              
                                                                               
 100  CONTINUE                                                                  
      DO 200 I=1,N                                                              
      PFUN(I)=APFUN(N-I+1)                                                      
 200  CONTINUE                                                                  
                                                                               
      ! INTEGRATION DE PFUN - SIMPSON'S RULE: QSF                                   
      CALL DQSF(DK,PFUN,SOL,N)                                                  
                                                                               
      ! VALEUR DU POTENTIEL                                                         
      ! CELA DOIT-IL ETRE CHANGE : NON                                                  
                                                                               
      ROC=DVS(IAG)*DEXP(R0/HL(IAG))                                             
      PHI(IAG)=SOL(N)*4.*PI*G*ROC/(ALP*ALP*Rgal)                                   
      RETURN                                                                    
      END                                                                       
                                                                               
      ! CALCUL DE LA FONCTION ZII2 POUR LE DISQUE EXPONENTIEL GENERALISE              
      FUNCTION ZII2(QR,Rgal,Zgal,IAG)                                                     
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
                                                                               
      ! ATTENTION DE MEME QUE ZII CETTE QUANTITE EST DIVISEE PAR DEUX SI ON LA        
      ! COMPARE AUX DEVELOPPMENT ANALYTIQUE  ( LE DEUX SE RETROUVE DANS POEXP         
      ! OU POEXP2 :   ON A UN 4 PI ROC  AU LIEU DE 2 PI ROC  )                         
                                                                               
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      COMMON/EXPGEN/XXL(15)                                                     
      XL = XXL(IAG)                                                             
      BET=1/HH(IAG)                                                             
      ALP=1/HL(IAG)                                                             
      IF(QR.EQ.0.) THEN                                                         
        ZII2=0.                                                                 
        GO TO 99                                                                
      END IF                                                                    
                                                                               
      XK = QR/Rgal                                                                 
      IF( DABS(Zgal).LT.XL ) THEN                                                   
         IF(DABS(XK-BET).GE.1.E-10) THEN                                          
            IF(XK*XL.LT.100. ) THEN                                             
            ZIIA= 2.*BET * DCOSH(XK*DABS(Zgal)) * DEXP(-XK*XL) / (XK*XK)              
            ZIIA= ZIIA * DEXP(-BET*XL) * (BET/(XK+BET) + 1./(XK*XL))             
            ELSE                                                                
            ZIIA=0.                                                             
            ENDIF                                                               
          ZIIB = DEXP(-BET*XL) / (XK)                                            
          ZIIB = ZIIB * (  2. + BET*XL - BET*(Zgal*Zgal+2./(XK*XK))/XL  )             
          ZII2=.5*(ZIIA+ZIIB) * DEXP(BET*XL) / (1.+BET*XL/2.)                    
        ELSE                                                                    
          WRITE(6,6666) QR, Rgal, BET, XK                                                        
6666      FORMAT(' PROBLEME DANS ZII2:  XK-BET LT 1.E-8')                       
          STOP                                                                  
        ENDIF                                                                   
      ENDIF                                                                     
      IF( DABS(Zgal).GE.XL ) THEN                                                   
          IF(XK*DABS(Zgal).LE.100. .AND. XK*XL.LE.100.)  THEN                       
            ZIIB= DEXP(-BET*XL) * DEXP(-XK*DABS(Zgal)) * DEXP(-XK*XL)                  
            ZIIB= ZIIB * (BET/(XK*XK))                                          
            ZIIB= ZIIB * (BET/(XK+BET) +1./(XK*XL))                             
            ZIIC= DEXP(-BET*XL) * DEXP(-XK*DABS(Zgal)) * DEXP(+XK*XL)                  
            ZIIC= ZIIC * (-BET/(XK*XK))                                         
            ZIIC= ZIIC * (BET/(XK-BET) +1./(XK*XL))                             
            ELSE                                                                
            ZIIB=0.                                                             
            ZIIC=0.                                                             
          ENDIF                                                                 
          IF(BET*DABS(Zgal).LT.100.) THEN                                           
            ZIIA= 2.*XK * DEXP(-BET*DABS(Zgal)) / (XK*XK-BET*BET)                    
            ELSE                                                                
            ZIIA=0.                                                             
          ENDIF                                                                 
          ZII2 =.5*(ZIIA + ZIIB + ZIIC) * DEXP(BET*XL) / (1.+BET*XL/2.)          
      ENDIF                                                                     
 99   RETURN                                                                    
      END                                                                       
                                                                               
                                                                                                                                                              
      SUBROUTINE FELL(IAG,Rgal,Zgal, FOR)                                                 
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)                                        
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      double precision kp1,km1,kp2,km2,kp3,km3
      COMMON/DISC/kp1,km1,kp2,km2,kp3,km3,rdis,rhole,EXC(15),iaghole
      COMMON/BORNINT/BM,BP,N1,N2,N,NN
      DIMENSION  PFUN(10000), SOL(10000)                                                                                                      
      DATA G/4.29569D-3/,  PI/3.14159265358D0/                                        
                                                                               
      AE = DASIN( E(IAG) )                                                       
      EX = E(IAG)                                                               
      CTE = 4.D0*PI*G* DSQRT(1-EX*EX) / (EX*EX*EX)                               
      ! write(6,*) e
      if(ex.eq.0.and.iag.ne.10.and.iag.ne.11) then
         write(6,*) 'FELL: iag, e ',iag,e(iag)
         stop
      endif
      ! WRITE(6,*) NN                                           
      DO 1 I = 1, NN
         IF(NN.GT.1) then
            DBET = AE / FLOAT( NN-1 )                                                 
            BET =  FLOAT( I-1 ) * DBET                                                
         ELSE
            BET= AE
         ENDIF
         SB = DSIN( BET )                                                           
         ALPHA = Rgal * SB / E(IAG)   
         IF(IAG.LE.7) then      !disc
            DEN = RHODISC2015C(IAG, rgal, 0.d0)
            !write(65,*) iag,r,z,e(iag),nn,alpha,den,sb
      ! elseif(IAG.EQ.9.and.kle.ne.5) then  !stellar halo
      ! DEN = RHOHALO(IAG,ALPHA)
         elseif(IAG.EQ.9.and.kle.eq.5) then  !stellar halo Hernquist
            DEN = RHOHALOH(IAG,ALPHA,rchalo)
      ! write(64,*) iag,r,z,e(iag),nn,alpha,den
         elseif(iag.eq.11.or.iag.eq.10) then
      ! write(6,*) 'FELL: IAG = ',IAG
            stop
         else
      ! write(6,*) 'bad try FELL, population not ellipsoid ',iag
            stop
         endif

1     PFUN(I) = DEN * SB * SB                                                                               
                                                                                
      CALL DQSF( DBET, PFUN, SOL, NN)  !no action if NN<3                                        
      FOR = SOL(NN) * Rgal * CTE                                                   
      ! WRITE(6,*) SOL(NN),Rgal 

      RETURN                                                                    
      END                                                                       

! Aqui se calcula la fuerza para las componentes axisimetricas como en Robin et al. (2014)  
! CALCUL LA FORCE POUR UNE DISTRIBUTION ELLIPSOIDALE DE DENSITE                
! E  EXCENTRICITE                                                              
! REF.   STARS AND STELLAR SYSTEMS                                              
! Annie - New version with cylindrical hole in the center
!------------------------------------------------------------------
      SUBROUTINE FELLDISC(kplus,IAG,ELL,Rgal,Zgal, FOR)                                                 
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)                                        
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      double precision kp1,km1,kp2,km2,kp3,km3
      COMMON/DISC/kp1,km1,kp2,km2,kp3,km3,rdis,rhole,EXC(15),iaghole
      COMMON/BORNINT/BM,BP,N1,N2,N,NN
      DIMENSION  PFUN(10000), SOL(10000)                                                                                                      
      DATA G/4.29569D-3/,  PI/3.14159265358D0/
      integer kplus
      real*8 a01, a02, d01, d02, d01old, d02old
      
        ! a01=(R0*R0+zed0*zed0/(exc(iag)*exc(iag)))/(kp1*kp1)
        ! d01=DEXP(-a01)
        ! a02=(R0*R0+zed0*zed0/(exc(iag)*exc(iag)))/(kp2*kp2)
        ! d02=DEXP(0.5D0-dsqrt(0.25d0+a02))
        a01=(R0*R0+zed0*zed0/(e(iag)*e(iag)))/(kp1*kp1)
        a01m=(R0*R0+zed0*zed0/(e(iag)*e(iag)))/(km1*km1) !il faut utiliser ell ou exc ? ell est modifi�e par les echelle de longueur, exc pas
        ! l'ancienne formule (Einasto) et son potentiel utilisait exc, donc pour tester qu'on retrouve il faut prendre exc
        d01=DEXP(-a01)-dexp(-a01m)

        a02=(R0*R0+zed0*zed0/(e(iag)*e(iag)))/(kp2*kp2)
        a02m=(R0*R0+zed0*zed0/(e(iag)*e(iag)))/(km2*km2)
        d02=DEXP(0.5D0-dsqrt(0.25d0+a02))-DEXP(0.5D0-dsqrt(0.25d0+a02m))

        d01old=d01
        d02old=d02

	! alternative ell
        a01=(R0*R0+zed0*zed0/(ell*ell))/(kp1*kp1)
        a01m=(R0*R0+zed0*zed0/(ell*ell))/(km1*km1) !il faut utiliser ell ou exc ? ell est modifi�e par les echelle de longueur, exc pas
        ! l'ancienne formule (Einasto) et son potentiel utilisait exc, donc pour tester qu'on retrouve il faut prendre exc
        d01=DEXP(-a01)-dexp(-a01m)

        a02=(R0*R0+zed0*zed0/(ell*ell))/(kp2*kp2)
        a02m=(R0*R0+zed0*zed0/(ell*ell))/(km2*km2)
        d02=DEXP(0.5D0-dsqrt(0.25d0+a02))-DEXP(0.5D0-dsqrt(0.25d0+a02m))

        ! write(87,*) exc(iag), ell, d01old, d01, d02old, d02  => c'est la meme chose parce que zed0<<r0

      ellell=dsqrt(1.d0-ell*ell)  ! new e(iag)

      AE = DASIN( ELLELL )
      EX = ELLELL
      CTE = 4.D0*PI*G* DSQRT(1-EX*EX) / (EX*EX*EX)                               
      ! write(6,*) e
      if(ex.eq.0.and.iag.ne.10.and.iag.ne.11) then
         !write(6,*) 'FELL: iag, e ',iag,ell
         stop
      endif
      ! WRITE(6,*) NN                                           
      DO 1 I = 1, NN
         IF(NN.GT.1) then
            DBET = AE / FLOAT( NN-1 )                                                 
            BET =  FLOAT( I-1 ) * DBET                                                
         ELSE
            BET= AE
         ENDIF
         SB = DSIN( BET )
         !ALPHA = Rgal * SB / ELL  !! attention c'est pour zgal=0  !!! a verifier si on utilise pour zgal!=0
         ALPHA= (Rgal*DSIN( BET ))**2 + (Zgal*DTAN(BET))**2
         IF(ELLELL.GT..1D-05) then
            ALPHA=(ALPHA**0.5)/ELLELL
        else
            ALPHA=(ALPHA**0.5)*1.D+06
        endif
        if(iag.eq.1.and.kplus.eq.1) then
            den=DEXP(-alpha*alpha/(KP1*KP1))*dvs(iag)/d01 ! ask Annie, I not sure that alpha*alpha
        elseif(iag.eq.1.and.kplus.eq.-1) then
            den=DEXP(-alpha*alpha/(KM1*KM1))*dvs(iag)/d01
        elseif(iag.le.7.and.kplus.eq.1) then
            den=DEXP(0.5D0-dsqrt(0.25d0+alpha*alpha/(kp2*kp2)))*dvs(iag)/d02
        elseif(iag.le.7.and.kplus.eq.-1) then
            den=DEXP(0.5D0-dsqrt(0.25d0+alpha*alpha/(km2*km2)))*dvs(iag)/d02
        endif

         !write(56,*) iag,kplus,ell,exc(iag),rgal,zgal,den,rhodisc2015c(iag,alpha),d01,d02
         PFUN(I) = DEN * SB * SB             !a verifier si on utilise pour zgal!=0
 1    continue
                                                                        
      CALL DQSF( DBET, PFUN, SOL, NN)  !no action if NN<3                                        
                                                                                
      FOR = SOL(NN) * Rgal * CTE                                                   
      !WRITE(86,*) NN,Rgal,zgal,kplus,for

      RETURN                                                                    
      END                                                                       

! CALCUL LA FORCE D'UN DISQUE EXPONENTIEL                                      
! HL ET HH  ECHELLE DE LONGUEUR ET DE HAUTEUR                                  
! METHODE DERIVEE DE TOOMRE ET CASERTANO
! Reference: Bienayme et al. (1987)
!------------------------------------------------------------------                                        
       SUBROUTINE FEXP(IAG,Rgal,Zgal, FOR)                                                
       IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
       DOUBLE PRECISION MMBSJ1                                                   
       REAL*8 K                                                                  
       COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
       COMMON/BORNINT/BM,BP,N1,N2,N,NN
       DIMENSION PFUN(10000), SOL(10000),APFUN(10000)                           
       EXTERNAL RHO                                                             
       EXTERNAL MMBSJ1                                                          
                                                                               
                                                                               
      ! PI, CTE DE GRAVITATION                                                      
      DATA PI/3.14159265358D0/, G/4.29569D-3/                                         
                                                                               
      ! CALCUL DE L'INTEGRALE DE TOOMRE POUR LE DISQUE EXPONENTIEL                   
      ! D'EPAISSEUR 1/B                                                             
      !                        RHO   =  EXP(-ALP*R) * EXP(-BET*Z)                                                                                                    
      !   F =4PI*G*R*R*ALP*RHOC*SOMM( J1(KR)K/(ALP**2+K**2)*(K+BET)) DE 0 A L'INF.    
                                                                               
                                                                               
      IF(DVS(IAG).LT.1.D-10) FOR = 0.D0                                         
      IF(DVS(IAG).LT.1.D-10) RETURN                                             
                                                                               
                                                                               
      ! PAS D'INTEGRATION                                                            
      DK = BP / FLOAT(N-1)                                                      
                                                                               
      ! INIT                                                                          
      ALP = 1.D0 / HL(IAG)                                                      
      BET = 1.D0 / HH(IAG)                                                      
      Q = ALP * Rgal                                                               
      QB = BET *Rgal                                                               
                                                                               
      ! CALCUL DE F(Q) = SOMM( J1(K)*K/(QB+K)*(Q*Q+K*K)**(1.5)  )                    
                                                                               
      DO 100 I = 1,N                                                            
      K = (I-1) * DK                                                            
      BJ = MMBSJ1(K, IER)                                                       
      IF(IER.NE.0) WRITE(6,667) IER , I                                         
667   FORMAT(' ERREUR FCT BESS.  IERR ET I :',  I3,5X,I5)                       
      APFUN(I) = BJ * K /(Q*Q+K*K)**1.5                                         
      APFUN(I) = APFUN(I) / ( K + QB )                                          
100   CONTINUE                                                                  
                                                                                
      DO 101 I=1,N                                                              
      PFUN(I)=APFUN(N-I+1)                                                      
 101  CONTINUE                                                                  
                                                                               
      ! INTEGRATION DE PFUN - SIMPSON'S RULE                                        
      CALL DQSF(DK  , PFUN, SOL, N)  
                                           
C   VALEUR DE LA DENSITE CENTRALE RHOC                                          
       DCdensity=DVS(IAG)*DEXP(R0/HL(IAG))                                           
       FOR = SOL(N) * 4.D0 * PI * G * Rgal*Rgal * ALP * DCdensity                          
      RETURN                                                                    
      END                                                                       
                                                                               
! CALCUL LA FORCE D UN DISQUE EXPONENTIEL                                      
! HL ET HH  ECHELLE DE LONGUEUR ET DE HAUTEUR                                  
! METHODE DERIVEE DE TOOMRE ET CASERTANO                                       
! Reference: Bienayme et al. (1987)
!------------------------------------------------------------------
      SUBROUTINE FEXP2(IAG,Rgal,Zgal, FOR)                                               
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      DOUBLE PRECISION MMBSJ1                                                   
      EXTERNAL ZII2                                                             
      EXTERNAL RHO                                                              
      EXTERNAL MMBSJ1                                                           
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      COMMON/BORNINT/BM,BP,N1,N2,N,NN
      DIMENSION PFUN(10000), SOL(10000)                                         
      REAL*8 K                                                                  
      DATA PI/3.14159265358D0/, G/4.29569D-3/                                         
                                                                               
      ! CALCUL DE L'INTEGRALE DE TOOMRE POUR LE DISQE EXPONENTIEL GENER.            
      ! D'EPAISSEUR 1/B                                                             
      !                      RHO   =  EXP(-ALP*R) * EXP(-BET*Z)                                                                                                   
      ! F =4PI*G*R*R*ALP*RHOC*SOMM( J1(KR)K*ZII2/(ALP**2+K**2)**1.5))       INF.    
                                                                               
                                                                               
      IF(DVS(IAG).LT.1.D-10) FOR = 0.D0                                         
      IF(DVS(IAG).LT.1.D-10) RETURN                                             
                                                                               
      ! PAS D'INTEGRATION                                                            
      DK = BP / FLOAT(N-1)                                                      
      ! INIT                                                                         
      ALP = 1.D0 / HL(IAG)                                                      
      BET = 1.D0 / HH(IAG)                                                      
      Q = ALP * Rgal                                                               
      QB = BET *Rgal                                                               
                                                                               
      ! CALCUL DE F(Q) = SOMM( J1(K)*K*ZII()/((Q*Q+K*K)**(1.5)  )                    
      DO 100 I = 1, N                                                           
      QR= (I-1) * DK                                                            
      BJ = MMBSJ1(QR, IER)                                                      
      IF(IER.NE.0) WRITE(6,667) IER , I                                         
667   FORMAT(' ERREUR FCT BESS.  IERR ET I :',  I3,5X,I5)                       
      POM=1./((1.+(QR/(ALP*Rgal))**2)**(1.5))                                      
      PFUN(N-I+1) = BJ * (QR/Rgal)*POM*ZII2(QR,Rgal,Zgal,IAG)                                
100   CONTINUE                                                                  
                                                                               
      ! INTEGRATION DE PFUN - SIMPSON'S RULE                                        
      CALL DQSF(DK  , PFUN, SOL, N)                                             
                                                                               
      ! VALEUR DE LA DENSITE CENTRALE RHOC                                          
      DCdensity=DVS(IAG)*DEXP(R0/HL(IAG))                                           
      ! WRITE(6,*) RHOC                                                          
      ! WRITE(6,777) SOL(N), ALP, RHOC                                           
777   FORMAT(1X,3E20.5)                                                         
C                                                                               
C FORCE                                                                         
       FOR = SOL(N) * 4.D0 * PI * G * DCdensity / (ALP*ALP*Rgal)                        
      RETURN                                                                    
      END                                                                       

      ! Not working ... Ask Annie ? , I am using a new version ..
      SUBROUTINE FCORON(IAG,Rgal,Zgal, FOR) ! Not working ... Ask Annie ? , I am using a new version ..                                            
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                      
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      COMMON/CORO/DCcore,DCdensity,CMcore,CMdensity,EXPOCO,EPSICO                          
      DATA G/4.29569D-3/ , PI/3.14159265358D0/                                        
      RS = Rgal / DCcore                                                              
      FOR = 4.*PI*G*DCdensity*DCcore* ( RS-ATAN(RS) ) / (RS*RS)                         
      RETURN                                                                   
      END                                                                      

!-------------------------------------------------------------------------------------------------------------------------------------------------------
! All the functions and subroutines presented here were tested in great detail.  
! Please let us know if you have any questions or suggestions: 
!     [1] Jose Fernandez <jfernandez@obs-besancon.fr> and/or <jfernandezt87@gmail.com>
!     [2] Annie Robin <annie@obs-besancon.fr> 
!
!-------------------------------------------------------------------------------------------------------------------------------------------------------
! Update 2016 -->        Phi(x,y,z) and F(x,y,z): Stellar Halo, Central Mass and Dark Matter Halo    
!-------------------------------------------------------------------------------------------------------------------------------------------------------
!*******************************************************************************************************************************************************
! ---> START THICK DISK AND ISM  ***********************************************************************************************************************
!*******************************************************************************************************************************************************
! PROGRAM SUBROUTINE Smith15(IAG,Rgal,Zgal,PoTSm15, FRTSm15, FZTSm15)
! Last update: 2016, March 07
! Written by: J. G. Fernandez-Trincado
! Reference: Flynn et al. (1991), Smith et al. (2015), and Robin et al. (2014)
! Output: Potential, Force in R, and Force in Z. 
! Units: Potential = (km/s)^2 , Force = (km/s)^2 * (1/pc)
! Comments: Thick disk with sech^2 shape and ISM with exponential shape.  
!*******************************************************************************************************************************************************
        SUBROUTINE MN75(aMNi,bMNi,MdMNi,Rdinput,Rsun,MdMN,G,Rgal,Zgal, PhiMN16, FRMN16, FZMN16)  ! Miyamoto & Nagai (1975) density.  
        IMPLICIT DOUBLE PRECISION(A-H,O-Z)
        DOUBLE PRECISION aMNi,bMNi,MdMNi, Asun, MassMN, MdMN, num1, den1, rhoMN16, PhiMN16, FRMN16, FZMN16, G
        DOUBLE PRECISION aMN, bMN, Rdinput, Rsun
        aMN     = aMNi*Rdinput 
        bMN     = bMNi*Rdinput
        Asun    = (4.*3.141592653589793 * ( (Rsun*Rsun + ((aMN + bMN)**2.))**(5./2.) )*bMN*bMN*bMN )  /  ( bMN*bMN*(  aMN*Rsun*Rsun  + (  (aMN + 3.*bMN)*(( aMN + bMN )**2. )  )  ) )
        MassMN  = MdMNi*MdMN*Asun 
        num1    = (bMN*bMN)*(   aMN*Rgal*Rgal   +   ( aMN  + 3.*DSQRT(Zgal*Zgal + bMN*bMN) )*(  aMN  + DSQRT( Zgal*Zgal + bMN*bMN ))*( aMN  + DSQRT( Zgal*Zgal + bMN*bMN )  ) )
        den1    = (4.*3.141592653589793)*((   Rgal*Rgal   +  ( aMN  + DSQRT( Zgal*Zgal + bMN*bMN) )*( aMN  + DSQRT( Zgal*Zgal + bMN*bMN) ) )**(5./2.))*((Zgal*Zgal + bMN*bMN)**(3./2.))
        rhoMN16 = (num1/den1)*MassMN
        PhiMN16 = +MassMN*G*(1.  /  DSQRT( Rgal*Rgal  + ( aMN + DSQRT(Zgal*Zgal  + bMN*bMN ) )*(aMN + DSQRT(Zgal*Zgal  + bMN*bMN )))) ! Potential, (km/s)**2
        FRMN16  = +(MassMN*G*Rgal) /  (( Rgal*Rgal  +   (  aMN  + DSQRT(Zgal*Zgal + bMN*bMN))**2.)**(3./2.))                          ! (km/s)^2 * (1/pc) 
        FZMN16  = +(MassMN*G*Zgal*( aMN + DSQRT( Zgal*Zgal + bMN*bMN ))) / ((( Zgal*Zgal + bMN*bMN )**(1./2.) ) * ( ( Rgal*Rgal + ( aMN + DSQRT( Zgal*Zgal + bMN*bMN ) )**2. )**(3./2.) ) )
        RETURN
        END
        SUBROUTINE Smith15(IAG,Rgal,Zgal,PoTSm15, FRTSm15, FZTSm15)
        IMPLICIT DOUBLE PRECISION (A-H,O-Z)  
        EXTERNAL MN75
	COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
        DOUBLE PRECISION PMN1, PMN2, PMN3, Pa1, Pa2, Pa3, TableSm, PoTSm15, FRTSm15, FZTSm15
        DOUBLE PRECISION Xsm, slh, rholocal, HzZ0, Rsun, MdMN, G, Rdinput
        Rdinput  = HL(IAG)  !4500.       ! Rd
        rholocal = DVS(IAG) !2.1E-2      ! Local density in the solar position, units = (M_sun / pc^3)
        HzZ0     = HH(IAG)  !140.
        Rsun     = 8000.
        MdMN     = rholocal
        G        = 4.29569E-3   ! pc*((km/sg)**2)/Msun
        TableSm  = 2            ! Selection Table from Smith et al. (2015)
        slh      = HzZ0/Rdinput ! hz/Rd (exponential disk) or z0/Rd (sech^2 disk)
        IF(IAG.eq.13) THEN 
        ! Radially exponential disk (Robin et al. 2003)
            Xsm = (-0.269*slh**3.)+(1.080*slh**2.)+(1.092*slh)  ! b/Rd
        ELSE
        ! sech^2 disk (Robin et al. 2014)
            Xsm = (-0.033*slh**3.)+(0.262*slh**2.)+(0.659*slh)  ! b/Rd
        ENDIF
        IF(TableSm.EQ.1) THEN
           !Table 1 from Smith et al. (2015), negative densities 
           PMN1 = (-0.0090*Xsm**4.)+(0.0640*Xsm**3. )+(-0.1653*Xsm**2.)+( 0.1164*Xsm)+(  1.9487)
           PMN2 = ( 0.0173*Xsm**4.)+(-0.0903*Xsm**3.)+( 0.0877*Xsm**2.)+( 0.2029*Xsm)+( -1.3077)
           PMN3 = (-0.0051*Xsm**4.)+(0.0287*Xsm**3. )+(-0.0361*Xsm**2.)+(-0.0544*Xsm)+(  0.2242)
           Pa1  = (-0.0358*Xsm**4.)+(0.2610*Xsm**3. )+(-0.6987*Xsm**2.)+(-0.1193*Xsm)+(  2.0074)
           Pa2  = (-0.0830*Xsm**4.)+(0.4992*Xsm**3. )+(-0.7967*Xsm**2.)+(-1.2966*Xsm)+(  4.4441)
           Pa3  = (-0.0247*Xsm**4.)+(0.1718*Xsm**3. )+(-0.4124*Xsm**2.)+(-0.5944*Xsm)+(  0.7333)
        ELSE
           ! Table 2 from Smith et al. (2015), positive densities at all positions
           PMN1 = ( 0.0036*Xsm**4.)+(-0.0330*Xsm**3.)+( 0.1117*Xsm**2.)+(-0.1335*Xsm)+( 0.1749)  ! MN1/Md
           PMN2 = (-0.0131*Xsm**4.)+(0.1090*Xsm**3. )+(-0.3035*Xsm**2.)+( 0.2921*Xsm)+(-5.7976)  ! MN2/Md
           PMN3 = (-0.0048*Xsm**4.)+(0.0454*Xsm**3. )+(-0.1425*Xsm**2.)+( 0.1012*Xsm)+( 6.7120)  ! MN3/Md
           Pa1  = (-0.0158*Xsm**4.)+(0.0993*Xsm**3. )+(-0.2070*Xsm**2.)+(-0.7089*Xsm)+( 0.6445)  ! a1/Rd
           Pa2  = (-0.0319*Xsm**4.)+(0.1514*Xsm**3. )+(-0.1279*Xsm**2.)+(-0.9325*Xsm)+( 2.6836)  ! a2/Rd
           Pa3  = (-0.0326*Xsm**4.)+(0.1816*Xsm**3. )+(-0.2943*Xsm**2.)+(-0.6329*Xsm)+( 2.3193)  ! a3/Rd    
        ENDIF 
	PoTSm15 = 0                                                                              ! Total Gravitational potential for (X,Y,Z), in (km/s)**2
        FRTSm15 = 0                                                                              ! Force FR, in (km/s)**2 * (1/pc)
        FZTSm15 = 0                                                                              ! Force Fz, in (km/s)**2 * (1/pc)
	CALL MN75(Pa1,Xsm,PMN1,Rdinput,Rsun,MdMN,G,Rgal,DABS(Zgal), PhiMN16, FRMN16, FZMN16) ! Firt
	PoTSm15 = PoTSm15 + PhiMN16    ! Potential 1 from the scaled Miyamoto & Nagai (1975) density 
        FRTSm15 = FRTSm15 + FRMN16     ! Force in R 1 from the scaled Miyamoto & Nagai (1975) density 
        FZTSm15 = FZTSm15 + FZMN16     ! Force in z 1 from the scaled Miyamoto & Nagai (1975) density 
	CALL MN75(Pa2,Xsm,PMN2,Rdinput,Rsun,MdMN,G,Rgal,DABS(Zgal), PhiMN16, FRMN16, FZMN16) ! Second 
	PoTSm15 = PoTSm15 + PhiMN16    ! Potential 2 from the scaled Miyamoto & Nagai (1975) density 
        FRTSm15 = FRTSm15 + FRMN16     ! Force in R 2 from the scaled Miyamoto & Nagai (1975) density 
	FZTSm15 = FZTSm15 + FZMN16     ! Force in z 1 from the scaled Miyamoto & Nagai (1975) density 
	CALL MN75(Pa3,Xsm,PMN3,Rdinput,Rsun,MdMN,G,Rgal,DABS(Zgal), PhiMN16, FRMN16, FZMN16) ! Third
	PoTSm15 = PoTSm15 + PhiMN16    ! Potential 3 from the scaled Miyamoto & Nagai (1975) density 
        FRTSm15 = FRTSm15 + FRMN16     ! Force in R 3 from the scaled Miyamoto & Nagai (1975) density 
        FZTSm15 = FZTSm15 + FZMN16     ! Force in z 1 from the scaled Miyamoto & Nagai (1975) density 
        RETURN 
        END
!*******************************************************************************************************************************************************
! ---> START STELLAR HALO ******************************************************************************************************************************
!*******************************************************************************************************************************************************
! SUBROUTINE: Potential and Force for a Hernquist Stellar Halo as Robin et al. (2014)
! Last update: 2016, February 03
! By J. G. Fernandez-Trincado
! Reference: Dehnen (1993), Robin et al. (2014)
! This is a spherical approximation to Robin et al. (2014) to minimize the computation time in test particle Simulations. 
! We will fit the new shape for a general gamma using Apogee data.
! Density(R,z) =    
!*******************************************************************************************************************************************************
      SUBROUTINE SHAprox2016(Rgal,Zgal,PhiSHAprox, FRSHAprox, FzSHAprox)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      DOUBLE PRECISION GraviConst, ExpgammaSH, RcoreSH, rhoSH, MassSH, R0
      DOUBLE PRECISION qshaprox, PhiSHAprox, FRSHAprox, FzSHAprox, mshaprox
      GraviConst    = 4.29569D-3               ! pc*((km/sg)**2)/Msun
      ExpgammaSH    = 1.
      RcoreSH       = 2100.                    ! Robin et al. (2014)
      qshaprox      = 0.77                     ! Robin et al. (2014)
      rhoSH         = DVS(9)                   ! Local mass density rho_0 in solar position , M/pc^3 
      MassSH        = (rhoSH*4.*3.141592653589793*(R0**ExpgammaSH)*((R0 + RcoreSH)**(4.-ExpgammaSH)))/(RcoreSH*(3.-ExpgammaSH))
      ! Warning: This solution is valid only for ExpgammaSH != 2.
      mshaprox      = DSQRT( Rgal*Rgal + (Zgal*Zgal/(qshaprox*qshaprox)))
      IF(mshaprox.EQ.0) THEN                        
        mshaprox    = 0.0000001              ! This is an arbitrary value close to zero to avoid numerical singularity
      ELSE
        mshaprox    = mshaprox
      ENDIF
      PhiSHAprox    = (((GraviConst*MassSH)/RcoreSH)*(1./(2.-ExpgammaSH)))*( 1. -  (( mshaprox / (mshaprox + RcoreSH))**(2. - ExpgammaSH))) ! Total Gravitational potential for (X,Y,Z), in (km/s)**2
      FRSHAprox     = (((GraviConst*MassSH)/RcoreSH)*(1./(2.-ExpgammaSH)))*( (2.-ExpgammaSH)*( (Rgal/ (mshaprox*( RcoreSH  + mshaprox  ) ))    -  ( Rgal /  ( (RcoreSH + mshaprox)**2  ) )   )*(( mshaprox / ( RcoreSH + mshaprox )  )**(1.-ExpgammaSH))   )   ! Force FR, in (km/s)**2 * (1/pc)
      FzSHAprox     = (((GraviConst*MassSH)/RcoreSH)*(1./(2.-ExpgammaSH)))*( (2.-ExpgammaSH)*( (Zgal/ (qshaprox*qshaprox*mshaprox*( RcoreSH  + mshaprox  ) ))    -  ( Zgal /  ( (qshaprox*qshaprox)*(RcoreSH + mshaprox)**2  ) )   )*(( mshaprox / ( RcoreSH + mshaprox )  )**(1.-ExpgammaSH))   )   ! Force Fz, in (km/s)**2 * (1/pc)
      RETURN
      END
!*******************************************************************************************************************************************************
! ---> START START DARK MATTER HALO ********************************************************************************************************************
!*******************************************************************************************************************************************************
! Program SUBROUTINE 
! Last update: 2016, February 03
! Written by: J. G. Fernandez-Trincado
! Reference: Begeman et al (1991) and Robin et al. (2003)
! Output: Potential, Force in R, and Force in Z. 
! Units: Potential = (km/s)^2 , Force = (km/s)^2 * (1/pc)
! Comments: Pseudo-Isothermal Dark Matter Halo
!*******************************************************************************************************************************************************
      SUBROUTINE DMCbgm(Rgal,Zgal,phidh2016,FRbgmdm,FZbgmdm)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/CORO/DCcore,DCdensity,CMcore,CMdensity,EXPOCO,EPSICO
      DOUBLE PRECISION GraviConst, dhcore, rhocore, constdh, rdh
      DOUBLE PRECISION phidh2016, FRbgmdm, FZbgmdm
      GraviConst    = 4.29569D-3  ! pc*((km/sg)**2)/Msun
      dhcore        =  DCcore     ! 993.7308    ! Fitting value
      rhocore       = DCdensity  ! 0.7436E+00  ! Msun/pc^3
      constdh       = 4.*3.141592653589793*GraviConst*rhocore
      rdh           = DSQRT( Rgal*Rgal + Zgal*Zgal )  
      IF(rdh.EQ.0) THEN ! To avoid numerical singularity
        rdh = 0.0000001        ! This is an arbitrary value.
      ELSE
        rdh = rdh
      ENDIF
!     Gravitational potential for (X,Y,Z), in (km/s)**2
      phidh2016   = -constdh*(dhcore*dhcore)*( 0.5*DLOG(1. + ( rdh / dhcore)*( rdh / dhcore)) + (dhcore/rdh)*DATAN( rdh / dhcore ) ) ! Total Gravitational potential for (X,Y,Z), in (km/s)**2
      FRbgmdm     =  constdh*(dhcore*dhcore)*( 0.5*(2.*Rgal / ( dhcore*dhcore + rdh*rdh ) ) +  ( Rgal / ( (rdh*rdh)*( ((rdh*rdh)/(dhcore*dhcore))  + 1. ) )   )  -   (  (dhcore*Rgal*DATAN(DSQRT(rdh*rdh)/dhcore ))    / ( (rdh*rdh)**(3./2.) )  ) )  ! Force FR, in (km/s)**2 * (1/pc)
      FZbgmdm     =  constdh*(dhcore*dhcore)*( 0.5*(2.*Zgal / ( dhcore*dhcore + rdh*rdh ) ) +  ( Zgal / ( (rdh*rdh)*( ((rdh*rdh)/(dhcore*dhcore))  + 1. ) )   )  -   (  (dhcore*Zgal*DATAN(DSQRT(rdh*rdh)/dhcore ))    / ( (rdh*rdh)**(3./2.) )  ) )  ! Force Fz, in (km/s)**2 * (1/pc)
      RETURN
      END
!*******************************************************************************************************************************************************
! ---> START CENTRAL MASS ******************************************************************************************************************************
!*******************************************************************************************************************************************************
! Program SUBROUTINE 
! Last update: 2016, February 03
! Written by: J. G. Fernandez-Trincado
! Reference: Miyamoto & Nagai (1975), Allen & Santillan (1991)
! Output: Potential, Force in R, and Force in Z.
! Units: Potential = (km/s)^2 , Force = (km/s)^2 * (1/pc)
! Comments: The circular velocity in agreement with Table 2 in Allen & Santillan (1991), in units of km/s**2.
!*******************************************************************************************************************************************************
      SUBROUTINE NewCM(Rgal,Zgal,POTTOTALcm,FRNCM16, FZNCM16)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/CORO/DCcore,DCdensity,CMcore,CMdensity,EXPOCO,EPSICO
      DOUBLE PRECISION MassCMdensity, bCMcore, POTTOTALcm, FRNCM16, FZNCM16, Factorcm, cmrgal6, cmzgal6
      bCMcore       = CMcore/1000.                
      Factorcm      = (4.*3.141592653589793*((2.*bCMcore*bCMcore)**(5./2.)))/(3.*bCMcore*bCMcore)
      MassCMdensity = (CMdensity*Factorcm)/2.32E7 
      cmrgal6       = Rgal/1000. ! kpc units
      cmzgal6       = Zgal/1000. ! kpc units
      PhiCM2016     = MassCMdensity / (DSQRT( cmrgal6*cmrgal6 + cmzgal6*cmzgal6 + bCMcore*bCMcore ))                         ! 3D Gravitation Potential in 100 km**2/s**2 units 
      POTTOTALcm    = PhiCM2016*100.                                                                                         ! Total Gravitational potential for (X,Y,Z), in (km/s)**2
      FRNCM16       = ((MassCMdensity*cmrgal6) / ( bCMcore*bCMcore + cmrgal6*cmrgal6 + cmzgal6*cmzgal6 )**(1.5) )*100./1000. ! Force FR, in (km/s)**2 * (1/pc)
      FZNCM16       = ((MassCMdensity*cmzgal6) / ( bCMcore*bCMcore + cmrgal6*cmrgal6 + cmzgal6*cmzgal6 )**(1.5) )*100./1000.    ! Force FZ, in (km/s)**2 * (1/pc)
      !WRITE(*,*) 'here ...',Zgal, cmzgal6, FZNCM16
      !Rgal = 1000
      !WRITE(*,*) MassCMdensity*2.32E7, DSQRT(FRNCM16*Rgal), (606.*2.32E7 / ((4./3.)*3.141592653589793*bCMcore*bCMcore*bCMcore*(2.**(5./2.))))
      RETURN
      END
!-------------------------------------------------------------------------------------------------------------------------------------------------------
! Update 2016 -->             Density laws: Thin disk, Thick disk, Central mass, Stellar halo, Dark halo, ISM
!-------------------------------------------------------------------------------------------------------------------------------------------------------
!*******************************************************************************************************************************************************
! ---> START STELLAR HALO ******************************************************************************************************************************
! Computing the Stellar halo density for (x,y,z) 
! Hernquist model
! Reference: Robin et al. (2014) and Fernandez-Trincado et al. (2015)
! New component: halo avec rayon de coeur fct de Hernquist avec alpha=1 et delta=xn
! Last update: 2016, February 03
! By J. G. Fernandez-Trincado
!*******************************************************************************************************************************************************
      FUNCTION rhohaloh(IAG,rgal3,zgal3)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)                                    
      DOUBLE PRECISION gamma_exp, beta_exp, alpha_exp, rcore_sh, psh, qsh
      DOUBLE PRECISION Ash, dsh0, dsh 
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      gamma_exp = 1.          ! This value is to follow Robin et al. (2014) and RR Lyrae analysis (Fernandez-Trincado)
      beta_exp  = 2.76 + 1.   ! In order to follow Robin et al. (2014), we use beta = n + 1, where n is the Robin value  
      alpha_exp = 1.          ! This value is to follow Robin et al. (2014) and RR Lyrae analysis (Fernandez-Trincado)
      rcore_sh  = 2100.       ! Robin et al. (2014)
      psh       = 1.
      qsh       = 0.77 
      Ash = DSQRT((rgal3*rgal3/(psh*psh)) + (zgal3*zgal3/(qsh*qsh))) ! Surface
 4    IF(Ash.LT.200.) then    ! assumed value equal to central mass to avoid numerical singularity on (R,z)=(0,0). 
        ALPHA0 = 200.
      else
        ALPHA0 = Ash
      endif
      alphasun = DSQRT( R0*R0 / (psh*psh) + (ZED0*ZED0/(qsh*qsh))) ! Surface in the Solar position 
      dsh0     = ((alphasun/rcore_sh)**(gamma_exp))*(( 1 + (alphasun/rcore_sh)**(alpha_exp))**((beta_exp - gamma_exp)/alpha_exp))
      dsh      = 1./(((Ash/rcore_sh)**(gamma_exp))*(( 1 + (Ash/rcore_sh)**(alpha_exp))**((beta_exp - gamma_exp)/alpha_exp)))
      rhohaloh = dsh0*dsh*DVS(IAG) ! rho(x,y,z) for the Stellar Halo
      RETURN
      END
!*******************************************************************************************************************************************************
! ---> START DARK MATTER HALO **************************************************************************************************************************
! Computing the Dark Matter halo density for (x,y,z) 
! Reference: Robin et al. (2003)
! Last update: 2016, February 03
! By J. G. Fernandez-Trincado
!*******************************************************************************************************************************************************
      FUNCTION rhodarkhalo2016(IAG,Rgal,Zgal)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z) 
      COMMON/CORO/DCcore,DCdensity,CMcore,CMdensity,EXPOCO,EPSICO
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE 
      DOUBLE PRECISION Rdarkhalo, rhodarkhalo2016
      Rdarkhalo       = DSQRT(Rgal*Rgal + Zgal*Zgal)                    ! Rdarkhalo is equal to a**2, see label Table 3 in Robin et al. (2003)
      rhodarkhalo2016 = DCdensity / (1. + ( Rdarkhalo / DCcore )**2 )   ! rho(x,y,z) for the Dark Matter Halo, central density and core radius constraint from Rotation curve
!     ddh0            = (1+(R0/DCcore)**2)                        ! Normalization 
!     Rdarkhalo       = DSQRT(Rgal*Rgal + Zgal*Zgal)              ! Rdarkhalo is equal to a**2, see label Table 3 in Robin et al. (2003)
!     rhodarkhalo2016 = (DVS(IAG)*ddh0)/(1+(Rdarkhalo/DCcore)**2) ! rho(x,y,z) for the Dark Matter Halo, central density and core radius constraint from Rotation curve
      RETURN
      END
!*******************************************************************************************************************************************************
! ---> START CENTRAL MASS ******************************************************************************************************************************
! Computing the Central Mass density for (x,y,z) 
! Reference: Miyamoto & Nagai (1975), Allen & Santillan (1991)
! Contribution in the total potential.
! Last update: 2016, February 03
! By J. G. Fernandez-Trincado
!*******************************************************************************************************************************************************
      FUNCTION rhocentralmass(IAG,Rgal,Zgal)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      COMMON/CORO/DCcore,DCdensity,CMcore,CMdensity,EXPOCO,EPSICO
      DOUBLE PRECISION bCMcore, Factorcm, MassCMdensity, Ralphacm, Acm, Ralph1, GMUAS1991, rhocentralmass
      bCMcore          = CMcore/1000.                ! To be fitted using the comments of Chemin et al. (2015)
      Factorcm         = (4.*3.141592653589793*((2.*bCMcore*bCMcore)**(5./2.)))/(3.*bCMcore*bCMcore)
      MassCMdensity    = (CMdensity*Factorcm)/2.32E7 ! To be fitted using the comments of Chemin et al. (2015)
      Ralphacm         = DSQRT(Rgal*Rgal + Zgal*Zgal)
      Acm              = ((3.0*bCMcore*bCMcore*MassCMdensity) / (4.0*3.141592653589793)) 
      Ralph1           = ( 1. / (( Rgal*Rgal +  Zgal*Zgal + bCMcore*bCMcore )**(5/2))) 
      GMUAS1991        = 2.32E7                                  ! GMUAS1991 = Galactic mass unit = 2.32x10**7 M_sun, see Allen & Santillan (1991) 
      rhocentralmass   = Acm*Ralph1*GMUAS1991                    ! rho(x,y,z) for the Central Mass, central density and core radius constraint from Rotation curve
      RETURN
      END
!*******************************************************************************************************************************************************
! ---> START THICK DISK ********************************************************************************************************************************
! Computing the Thick disk density for (x,y,z) 
! Reference: Robin et al. (2003)
! Annie: Version without flare for the potential
! Last update: 2016, February 02
! By J. G. Fernandez-Trincado
!*******************************************************************************************************************************************************
      FUNCTION rhothd(IAG,rgal3,zgal3)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z) 
      DOUBLE PRECISION  rhothd,rgal3, zgal3
      DOUBLE PRECISION  kflare,za,xl,alp,bet,cta,d0,ccc,rhowf1,hhvar
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      COMMON/EXPGEN/XXL(15)
 3    ZA=ABS(zgal3)
      XL=XXL(IAG)
      ALP=1./HL(IAG)      
      bet=1./hh(iag)
          CTA= -BET/(XL*(2.+BET*XL))
          d0=1./(cta*zed0**2. +1.)
      IF((rgal3-R0)*ALP.LT.174..AND.ZA*BET.LT.174.) THEN
        IF(ZA.GE.XL) THEN
          CCC=EXP(-(rgal3-R0)*ALP)*EXP(-ZA*BET)
          rhowf1 = CCC * EXP(XL*BET)/(1.+XL*BET*0.5)
          rhothd = d0*rhowf1*DVS(IAG) ! rho(x,y,z) for the Thick disk
        ELSE
          rhowf1 = (CTA*ZA*ZA + 1.) *EXP(-(rgal3-R0)*ALP)
          rhothd = d0*rhowf1*DVS(IAG) ! rho(x,y,z) for the Thick disk
        ENDIF
      ELSE
        rhothd=0.
      END IF

      RETURN
      END
!*******************************************************************************************************************************************************
! ---> START THIN DISK *********************************************************************************************************************************
! Computing the thin disk density for (x,y,z) 
! Cilindrical hole, modified version of Einasto (1979)
! Reference: In preparation ...
! Preliminary text can be found here: In preparation ...  
! Note: The real shape of the thin disk on the inner part of the Milky Way is unknow, 
!       we need more tracers to study this, interesting to start this from Apogee.    
! Last update: 2016, February 01
! By J. G. Fernandez-Trincado
!*******************************************************************************************************************************************************
      FUNCTION rhodisc2015c(IAG,rgal,zgal)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)                                    
      COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
      double precision kp1,km1,kp2,km2,kp3,km3
      double precision kp1i,km1i,kp2i,km2i,kp3i,km3i
      COMMON/DISC/kp1,km1,kp2,km2,kp3,km3,rdis,rhole,EXC(15),iaghole
      !**********************************************************************************
      ! Population 1, Age < 0.15 Gyr 
      ! Last update: February 25, 2016
      if(iag.eq.1) then
 1    KP1I     = 1./(KP1*KP1)
      KM1I     = 1./(KM1*KM1)
      EPSILP2  = (1.-e(iag)*e(iag))
      EP1      = kp1i/EPSILP2
      D1       = DEXP(-R0*R0*KP1I - ZED0*ZED0*EP1)
      D2       = DEXP(-R0*R0*KM1I - ZED0*ZED0*EP1)
      D0       = D1-D2
      alcapone = rgal*rgal*KP1I + zgal*zgal*EP1         ! Cilindrical hole contribution
      IF(alcapone.LT.174.6d0) THEN
        rho1=DEXP(-alcapone)
      ELSE
        rho1=DEXP(-174.6D0)
      ENDIF
      alcamone=rgal*rgal*KM1I + zgal*zgal*EP1
      IF(alcamone.LT.174.6d0) THEN
        rho2=DEXP(-alcamone)
      ELSE
        rho2=DEXP(-174.6D0)
      ENDIF
      rhodisc2015c=(DVS(IAG)/D0)*(rho1-rho2)            ! rho(x,y,z) for thin disk with Ages < 0.15 Gyr, including the cilindrical hole effect for each component
      RETURN
      !**************************************************************************************
      ! Population 2 to 7, Age > 0.15 Gyr
      ! Last update: February 25, 2016
      elseif(iag.ge.2.and.iag.le.7) then                ! Thind disk density ...
 2    XX=0.25                                           ! structural parameter of the Einasto model (see Einasto 1979)
      R02=R0*R0                                         ! Solar position, Ro = 8 kpc
      KP2I=1./(KP2*KP2)                                 ! factor of the hole positive
      KM2I=1./(KM2*KM2)                                 ! factor of the hole negative
      EPSILP2=(1.-e(iag)*e(iag))                        ! axis ratios
      EP2=kp2i/EPSILP2             
      D1=DEXP(0.5 -(XX+R02*KP2I+ ZED0*ZED0*EP2)**(0.5)) ! Positive density (see Fernandez-Trincado text)
      D2=DEXP(0.5 -(XX+R02*KM2I+ ZED0*ZED0*EP2)**(0.5)) ! Negative density (see Fernandez-Trincado text)
      D0=D1-D2                                          ! Normalization constant for each component of the thin disk
      A2P=Rgal*Rgal*KP2I + Zgal*Zgal*EP2                ! Positive surface (see Fernandez-Trincado text)
      alpha=sqrt(rgal*rgal+zgal*zgal/epsilp2)
      IF(A2P.LT.3.E+03) THEN
        P1=DEXP(0.5-(XX+A2P)**(0.5))
      ELSE
        P1=DEXP(0.5-(XX+3.E+03)**(0.5))
      ENDIF

      A2M=Rgal*Rgal*KM2I + Zgal*Zgal*EP2                ! Negative surface (see Fernandez-Trincado text)
      IF(A2M.LT.3.E+03) THEN
        P2=DEXP(0.5-(XX+A2M)**(0.5))
      ELSE
        P2=DEXP(0.5-(XX+3.E+03)**(0.5))
      ENDIF
      rhodisc2015c=(P1-P2)*DVS(IAG)/D0                  ! rho(x,y,z) for thin disk with Ages > 0.15 Gyr, including the cilindrical hole effect for each component
      !write(11111,*) 'iag, Density, D0, e(iag)', IAG, DVS(IAG), D0, e(iag)
      !if(iag.eq.7.and.zgal.eq.0.) write(67,*) rgal,kp2,km2,p1,p2,DEXP(0.5-(XX+alpha*alpha*km2I)**(0.5))
      RETURN
!*******************************************************************************************************************************************************
! ---> START ISM ***************************************************************************************************************************************
! Computing the density of the ISM for (x,y,z) 
! Reference: Robin et al. (2003)
! Annie: DISQUE FLAT EINASTO 1979 AMELIORE, MATIERE INTERSTELLAIRE
! Annie: ANCIENNES VALEURS:   KP=7.22176E+03   KM=5.77741E+03
! Annie: (mais pourrait etre appliqu�e a HI... si on avait plusieurs types
! Annie: de milieu absorbant)
! Last update: 2016, February 01
! By J. G. Fernandez-Trincado
!*******************************************************************************************************************************************************
      elseif(iag.eq.13) then
 13   KP3I=1./(KP3*KP3)
      KM3I=1./(KM3*KM3)
      D0=DEXP(-R0*R0*KP3I)-DEXP(-R0*R0*KM3I)
      ALP=ALPHA*ALPHA*KP3I
      IF(ALP.LT.74.6) THEN
        rho1=DEXP(-ALP)
      ELSE
        rho1=0.
      END IF
      ALP=ALPHA*ALPHA*KM3I
      IF(ALP.LT.74.6) THEN
        rho2=DEXP(-ALP)
      ELSE
        rho2=DEXP(-74.6D0)
      ENDIF
      rhodisc2015c = (DVS(IAG)/D0)*(rho1-rho2)        ! rho(x,y,z) for ISM
      RETURN
      else
         uuuuuu = 0.
         stop
      endif
      END
