C                                                                       48260010
C     ..................................................................48260020
C                                                                       48260030
C        SUBROUTINE DQSF                                                48260040
C                                                                       48260050
C        PURPOSE                                                        48260060
C           TO COMPUTE THE VECTOR OF INTEGRAL VALUES FOR A GIVEN        48260070
C           EQUIDISTANT TABLE OF FUNCTION VALUES.                       48260080
C                                                                       48260090
C        USAGE                                                          48260100
C           CALL DQSF (H,Y,Z,NDIM)                                      48260110
C                                                                       48260120
C        DESCRIPTION OF PARAMETERS                                      48260130
C           H      - DOUBLE PRECISION INCREMENT OF ARGUMENT VALUES.     48260140
C           Y      - DOUBLE PRECISION INPUT VECTOR OF FUNCTION VALUES.  48260150
C           Z      - RESULTING DOUBLE PRECISION VECTOR OF INTEGRAL      48260160
C                    VALUES. Z MAY BE IDENTICAL WITH Y.                 48260170
C           NDIM   - THE DIMENSION OF VECTORS Y AND Z.                  48260180
C                                                                       48260190
C        REMARKS                                                        48260200
C           NO ACTION IN CASE NDIM LESS THAN 3.                         48260210
C                                                                       48260220
C        SUBROUTINES AND FUNCTION SUBPROGRAMS REQUIRED                  48260230
C           NONE                                                        48260240
C                                                                       48260250
C        METHOD                                                         48260260
C           BEGINNING WITH Z(1)=0, EVALUATION OF VECTOR Z IS DONE BY    48260270
C           MEANS OF SIMPSONS RULE TOGETHER WITH NEWTONS 3/8 RULE OR A  48260280
C           COMBINATION OF THESE TWO RULES. TRUNCATION ERROR IS OF      48260290
C           ORDER H**5 (I.E. FOURTH ORDER METHOD). ONLY IN CASE NDIM=3  48260300
C           TRUNCATION ERROR OF Z(2) IS OF ORDER H**4.                  48260310
C           FOR REFERENCE, SEE                                          48260320
C           (1) F.B.HILDEBRAND, INTRODUCTION TO NUMERICAL ANALYSIS,     48260330
C               MCGRAW-HILL, NEW YORK/TORONTO/LONDON, 1956, PP.71-76.   48260340
C           (2) R.ZURMUEHL, PRAKTISCHE MATHEMATIK FUER INGENIEURE UND   48260350
C               PHYSIKER, SPRINGER, BERLIN/GOETTINGEN/HEIDELBERG, 1963, 48260360
C               PP.214-221.                                             48260370
C                                                                       48260380
C     ..................................................................48260390
C                                                                       48260400
      SUBROUTINE DQSF(H,Y,Z,NDIM)                                       !48260410
C                                                                       !48260420
C                                                                       !48260430
      DIMENSION Y(NDIM+1),Z(NDIM+1)                                     !48260440
      DOUBLE PRECISION Y,Z,H,HT,SUM1,SUM2,AUX,AUX1,AUX2                 !48260450
C                                                                       !48260460
      HT=.33333333333333333D0*H                                         !48260470
      IF(NDIM-5)7,8,1                                                   !48260480
C                                                                       !48260490
C     NDIM IS GREATER THAN 5. PREPARATIONS OF INTEGRATION LOOP          !48260500
    1 SUM1=Y(2)+Y(2)                                                    !48260510
      SUM1=SUM1+SUM1                                                    !48260520
      SUM1=HT*(Y(1)+SUM1+Y(3))                                          !48260530
      AUX1=Y(4)+Y(4)                                                    !48260540
      AUX1=AUX1+AUX1                                                    !48260550
      AUX1=SUM1+HT*(Y(3)+AUX1+Y(5))                                     !48260560
      AUX2=HT*(Y(1)+3.875D0*(Y(2)+Y(5))+2.625D0*(Y(3)+Y(4))+Y(6))       !48260570
      SUM2=Y(5)+Y(5)                                                    !48260580
      SUM2=SUM2+SUM2                                                    !48260590
      SUM2=AUX2-HT*(Y(4)+SUM2+Y(6))                                     !48260600
      Z(1)=0.D0                                                         !48260610
      AUX=Y(3)+Y(3)                                                     !48260620
      AUX=AUX+AUX                                                       !48260630
      Z(2)=SUM2-HT*(Y(2)+AUX+Y(4))                                      !48260640
      Z(3)=SUM1                                                         !48260650
      Z(4)=SUM2                                                         !48260660
      IF(NDIM-6)5,5,2                                                   !48260670
C                                                                       !48260680
C     INTEGRATION LOOP                                                  !48260690
    2 DO 4 I=7,NDIM,2                                                   !48260700
      SUM1=AUX1                                                         !48260710
      SUM2=AUX2                                                         !48260720
      AUX1=Y(I-1)+Y(I-1)                                                !48260730
      AUX1=AUX1+AUX1                                                    !48260740
      AUX1=SUM1+HT*(Y(I-2)+AUX1+Y(I))                                   !48260750
      Z(I-2)=SUM1                                                       !48260760
      IF(I-NDIM)3,6,6                                                   !48260770
    3 AUX2=Y(I)+Y(I)                                                    !48260780
      AUX2=AUX2+AUX2                                                    !48260790
      AUX2=SUM2+HT*(Y(I-1)+AUX2+Y(I+1))                                 !48260800
    4 Z(I-1)=SUM2                                                       !48260810
    5 Z(NDIM-1)=AUX1                                                    !48260820
      Z(NDIM)=AUX2                                                      !48260830
      RETURN                                                            !48260840
    6 Z(NDIM-1)=SUM2                                                    !48260850
      Z(NDIM)=AUX1                                                      !48260860
      RETURN                                                            !48260870
C     END OF INTEGRATION LOOP                                           !48260880
C                                                                       !48260890
    7 IF(NDIM-3)12,11,8                                                 !48260900
C                                                                       !48260910
C     NDIM IS EQUAL TO 4 OR 5                                           !48260920
    8 SUM2=1.125D0*HT*(Y(1)+Y(2)+Y(2)+Y(2)+Y(3)+Y(3)+Y(3)+Y(4))         !48260930
      SUM1=Y(2)+Y(2)                                                    !48260940
      SUM1=SUM1+SUM1                                                    !48260950
      SUM1=HT*(Y(1)+SUM1+Y(3))                                          !48260960
      Z(1)=0.D0                                                         !48260970
      AUX1=Y(3)+Y(3)                                                    !48260980
      AUX1=AUX1+AUX1                                                    !48260990
      Z(2)=SUM2-HT*(Y(2)+AUX1+Y(4))                                     !48261000
      IF(NDIM-5)10,9,9                                                  !48261010
    9 AUX1=Y(4)+Y(4)                                                    !48261020
      AUX1=AUX1+AUX1                                                    !48261030
      Z(5)=SUM1+HT*(Y(3)+AUX1+Y(5))                                     !48261040
   10 Z(3)=SUM1                                                         !48261050
      Z(4)=SUM2                                                         !48261060
      RETURN                                                            !48261070
C                                                                       !48261080
C     NDIM IS EQUAL TO 3                                                !48261090
   11 SUM1=HT*(1.25D0*Y(1)+Y(2)+Y(2)-.25D0*Y(3))                        !48261100
      SUM2=Y(2)+Y(2)                                                    !48261110
      SUM2=SUM2+SUM2                                                    !48261120
      Z(3)=HT*(Y(1)+SUM2+Y(3))                                          !48261130
      Z(1)=0.D0                                                         !48261140
      Z(2)=SUM1                                                         !48261150
   12 RETURN                                                            !48261160
      END                                                               !48261170
