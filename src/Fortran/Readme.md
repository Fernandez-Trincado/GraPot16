
[GraPot16](https://github.com/Fernandez-Trincado/GraPot16/blob/master/README.md) in Fortran version
===

Introduction
---

Inputs
---

The [input](https://github.com/Fernandez-Trincado/GraPot16/blob/master/src/Fortran/srcfortran/Inputs.for) is relationed with the Galactocentric coordinates in pc, Rsun = 8000 pc the Galactocentric radius of the Sun.

  * `Rgal = SQRT(X^2  + Y^2)`
  * `Zgal = Z`

Outputs
---

The output is relationed with the Galactic Gravitational Potential `Pot` in `(km/s)^2`, and `Force` in `(km/s)^2 (1/pc)`.

Galactic Potentials
---

We have adopted a righthanded coordinate system for `(U,V,W)`, so that they are positive in the directions of the galactic center, galactic rotation, and north galactic pole, respectively.

  * `Seven stellar populations of the Thin disk`:
   * Population 1:
   * Population 2:
   * Population 3:
   * Population 4:
   * Population 5:
   * Population 6:
  * `Two stellar populations of the Thick disk`:
   * Young:
   * Old:
  * `Interstellar Matter (ISM)`:
  * `Stellar Halo`:
  * `Pseudo-Isothermal Dark Matter Halo`:
  * `Central Mass`:

To run the program, type:
---
 * `Step 1`: Download the entire package at [srcfortran](https://github.com/Fernandez-Trincado/GraPot16/tree/master/src/Fortran/srcfortran)
 * `Step 2`: You need the configuration file `blockdata.dat`, this file will be available after the final publication. The entire program is available temporarily only within the collaboration. You can email me `jfernandez@obs-besancon.fr` and/or `jfernandezt87@gmail.com` or contact to the following persons [Section authors](https://github.com/Fernandez-Trincado/GraPot16/blob/master/README.md)
 * `Step 3`: `gfortran -fopenmp -fbounds-check -fcheck-array-temporaries -Wsurprising -ffixed-line-length-none -o `[GraPot16](https://github.com/Fernandez-Trincado/GraPot16/blob/master/README.md)` subpot2016.for  mmbsj0a.for mmbsj1a.for dqsf.for  ecbloc15b.for IC_Main_program.for Inputs.for`
 
If you have trouble grabbing or decoding the code please email me at **jfernandez@obs-besancon.fr** and/or **jfernandezt87@gmail.com**

Run `GraPot16`
---

`./GraPot16`

Description
---

 * [subpot2016.for](https://github.com/Fernandez-Trincado/GraPot16/blob/master/src/Fortran/srcfortran/subpot2016.for) Description here ...
 * [mmbsj0a.for](https://github.com/Fernandez-Trincado/GraPot16/blob/master/src/Fortran/srcfortran/mmbsj0a.for) Description here ...
 * [mmbsj1a.for](https://github.com/Fernandez-Trincado/GraPot16/blob/master/src/Fortran/srcfortran/mmbsj1a.for) Description here ...
 * [dqsf.for](https://github.com/Fernandez-Trincado/GraPot16/blob/master/src/Fortran/srcfortran/dqsf.for) Description here ...
 * [IC_Main_program.for](https://github.com/Fernandez-Trincado/GraPot16/blob/master/src/Fortran/srcfortran/IC_Main_program.for) Description here ...
 * [Inputs.for](https://github.com/Fernandez-Trincado/GraPot16/blob/master/src/Fortran/srcfortran/Inputs.for) Description here ...

Support
--

If you use `GraPot16 (Fortran version)` in your research, we would be grateful if you could include an acknowledgment in papers and/or presentations:

      "This research made use of GraPot16 (Fortran version), a community-developed core under the git 
       version-control system on GitHub. Funding for GraPot16 has been provided by Centre National 
       d'Etudes Spatiales (CNES) through the grant 0101973 and the R\'egion de Franche-Comt\'e and by the
       French Programme National de Cosmologie et Galaxies (PNCG)."

