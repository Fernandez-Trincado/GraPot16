!-------------------------------------------------------------------------------------------------------------------------------------------------------
! Update 2016 -->        Phi(x,y,z) and F(x,y,z): Thick disk and ISM
! Density law of the Thick disk: 
!             \rho(R,z) = rho_sun*exp(-R/Rd)*sech^2(-|z|/zo)
!
!             Constants:  
!             (a) Young Thick disk: IAG = 8
!                 rho_sun = DVS(IAG) = 0.850000E-02 M_sun/pc^3
!                 Rd      = HL(IAG)  = 2040.0 pc                
!                 zo      = HH(IAG)  =  345.0 pc
!
!             (b) Old Thick disk: IAG = 11 
!                 rho_sun = DVS(IAG) = 0.130000E-02 M_sun/pc^3                   
!                 Rd      = HL(IAG)  = 2919.0 pc
!                 zo      = HH(IAG)  =  795.0 pc
!
! Density law of ISM:  IAG = 13
!             \rho(R,z) = rho_sun*exp(-R/Rd)*exp(-|z|/zo)
!
!             Constants: 
!             rho_sun = DVS(IAG) = 0.400000E-01   M_sun/Pc^2
!             Rd      = HL(IAG)  = 4500.0 pc     
!             zo      = HH(IAG)  =  140.0 pc
!
!-------------------------------------------------------------------------------------------------------------------------------------------------------
!*******************************************************************************************************************************************************
! ---> START THICK DISK AND ISM  ***********************************************************************************************************************
!*******************************************************************************************************************************************************
! PROGRAM SUBROUTINE Smith15(IAG,Rgal,Zgal,PoTSm15, FRTSm15, FZTSm15)
! Last update: 2016, March 07
! Written by: J. G. Fernandez-Trincado
! Reference: Flynn et al. (1991), Smith et al. (2015), and Robin et al. (2014)
! Outputs:
!         Potential: PoTSm15
!        Force in R: FRTSm15  
!        Force in z: FZTSm15
! Units: Potential = (km/s)^2 , Force = (km/s)^2 * (1/pc)
!*******************************************************************************************************************************************************
        SUBROUTINE MN75(aMNi,bMNi,MdMNi,Rdinput,Rsun,MdMN,G,Rgal,Zgal, PhiMN16, FRMN16, FZMN16)  ! Miyamoto & Nagai (1975) density.  
        IMPLICIT DOUBLE PRECISION(A-H,O-Z)
        DOUBLE PRECISION aMNi,bMNi,MdMNi, Asun, MassMN, MdMN, num1, den1, rhoMN16, PhiMN16, FRMN16, FZMN16, G
        DOUBLE PRECISION aMN, bMN, Rdinput, Rsun
        aMN     = aMNi*Rdinput 
        bMN     = bMNi*Rdinput
        Asun    = (4.*3.141592653589793 * ( (Rsun*Rsun + ((aMN + bMN)**2.))**(5./2.) )*bMN*bMN*bMN )  /  ( bMN*bMN*(  aMN*Rsun*Rsun  + (  (aMN + 3.*bMN)*(( aMN + bMN )**2. )  )  ) )
        MassMN  = MdMNi*MdMN*Asun 
        num1    = (bMN*bMN)*(   aMN*Rgal*Rgal   +   ( aMN  + 3.*DSQRT(Zgal*Zgal + bMN*bMN) )*(  aMN  + DSQRT( Zgal*Zgal + bMN*bMN ))*( aMN  + DSQRT( Zgal*Zgal + bMN*bMN )  ) )
        den1    = (4.*3.141592653589793)*((   Rgal*Rgal   +  ( aMN  + DSQRT( Zgal*Zgal + bMN*bMN) )*( aMN  + DSQRT( Zgal*Zgal + bMN*bMN) ) )**(5./2.))*((Zgal*Zgal + bMN*bMN)**(3./2.))
        rhoMN16 = (num1/den1)*MassMN
        PhiMN16 = +MassMN*G*(1.  /  DSQRT( Rgal*Rgal  + ( aMN + DSQRT(Zgal*Zgal  + bMN*bMN ) )*(aMN + DSQRT(Zgal*Zgal  + bMN*bMN )))) ! Potential, (km/s)**2
        FRMN16  = +(MassMN*G*Rgal) /  (( Rgal*Rgal  +   (  aMN  + DSQRT(Zgal*Zgal + bMN*bMN))**2.)**(3./2.))                          ! (km/s)^2 * (1/pc) 
        FZMN16  = +(MassMN*G*Zgal*( aMN + DSQRT( Zgal*Zgal + bMN*bMN ))) / ((( Zgal*Zgal + bMN*bMN )**(1./2.) ) * ( ( Rgal*Rgal + ( aMN + DSQRT( Zgal*Zgal + bMN*bMN ) )**2. )**(3./2.) ) )
        RETURN
        END
        SUBROUTINE Smith15(IAG,Rgal,Zgal,PoTSm15, FRTSm15, FZTSm15)
        IMPLICIT DOUBLE PRECISION (A-H,O-Z)  
        EXTERNAL MN75
	COMMON/C1/R0,ZED0,E(15),DVS(15),HL(15),HH(15),XN,KLE
        DOUBLE PRECISION PMN1, PMN2, PMN3, Pa1, Pa2, Pa3, TableSm, PoTSm15, FRTSm15, FZTSm15
        DOUBLE PRECISION Xsm, slh, rholocal, HzZ0, Rsun, MdMN, G, Rdinput
        Rdinput  = HL(IAG)  !4500.       ! Rd
        rholocal = DVS(IAG) !2.1E-2      ! Local density in the solar position, units = (M_sun / pc^3)
        HzZ0     = HH(IAG)  !140.
        Rsun     = 8000.
        MdMN     = rholocal
        G        = 4.29569E-3   ! pc*((km/sg)**2)/Msun
        TableSm  = 2            ! Selection Table from Smith et al. (2015)
        slh      = HzZ0/Rdinput ! hz/Rd (exponential disk) or z0/Rd (sech^2 disk)
        IF(IAG.eq.13) THEN 
        ! Radially exponential disk (Robin et al. 2003)
            Xsm = (-0.269*slh**3.)+(1.080*slh**2.)+(1.092*slh)  ! b/Rd
        ELSE
        ! sech^2 disk (Robin et al. 2014)
            Xsm = (-0.033*slh**3.)+(0.262*slh**2.)+(0.659*slh)  ! b/Rd
        ENDIF
        IF(TableSm.EQ.1) THEN
           !Table 1 from Smith et al. (2015), negative densities 
           PMN1 = (-0.0090*Xsm**4.)+(0.0640*Xsm**3. )+(-0.1653*Xsm**2.)+( 0.1164*Xsm)+(  1.9487)
           PMN2 = ( 0.0173*Xsm**4.)+(-0.0903*Xsm**3.)+( 0.0877*Xsm**2.)+( 0.2029*Xsm)+( -1.3077)
           PMN3 = (-0.0051*Xsm**4.)+(0.0287*Xsm**3. )+(-0.0361*Xsm**2.)+(-0.0544*Xsm)+(  0.2242)
           Pa1  = (-0.0358*Xsm**4.)+(0.2610*Xsm**3. )+(-0.6987*Xsm**2.)+(-0.1193*Xsm)+(  2.0074)
           Pa2  = (-0.0830*Xsm**4.)+(0.4992*Xsm**3. )+(-0.7967*Xsm**2.)+(-1.2966*Xsm)+(  4.4441)
           Pa3  = (-0.0247*Xsm**4.)+(0.1718*Xsm**3. )+(-0.4124*Xsm**2.)+(-0.5944*Xsm)+(  0.7333)
        ELSE
           ! Table 2 from Smith et al. (2015), positive densities at all positions
           PMN1 = ( 0.0036*Xsm**4.)+(-0.0330*Xsm**3.)+( 0.1117*Xsm**2.)+(-0.1335*Xsm)+( 0.1749)  ! MN1/Md
           PMN2 = (-0.0131*Xsm**4.)+(0.1090*Xsm**3. )+(-0.3035*Xsm**2.)+( 0.2921*Xsm)+(-5.7976)  ! MN2/Md
           PMN3 = (-0.0048*Xsm**4.)+(0.0454*Xsm**3. )+(-0.1425*Xsm**2.)+( 0.1012*Xsm)+( 6.7120)  ! MN3/Md
           Pa1  = (-0.0158*Xsm**4.)+(0.0993*Xsm**3. )+(-0.2070*Xsm**2.)+(-0.7089*Xsm)+( 0.6445)  ! a1/Rd
           Pa2  = (-0.0319*Xsm**4.)+(0.1514*Xsm**3. )+(-0.1279*Xsm**2.)+(-0.9325*Xsm)+( 2.6836)  ! a2/Rd
           Pa3  = (-0.0326*Xsm**4.)+(0.1816*Xsm**3. )+(-0.2943*Xsm**2.)+(-0.6329*Xsm)+( 2.3193)  ! a3/Rd    
        ENDIF 
	PoTSm15 = 0                                                                              ! Total Gravitational potential for (X,Y,Z), in (km/s)**2
        FRTSm15 = 0                                                                              ! Force FR, in (km/s)**2 * (1/pc)
        FZTSm15 = 0                                                                              ! Force Fz, in (km/s)**2 * (1/pc)
	CALL MN75(Pa1,Xsm,PMN1,Rdinput,Rsun,MdMN,G,Rgal,DABS(Zgal), PhiMN16, FRMN16, FZMN16) ! Firt
	PoTSm15 = PoTSm15 + PhiMN16    ! Potential 1 from the scaled Miyamoto & Nagai (1975) density 
        FRTSm15 = FRTSm15 + FRMN16     ! Force in R 1 from the scaled Miyamoto & Nagai (1975) density 
        FZTSm15 = FZTSm15 + FZMN16     ! Force in z 1 from the scaled Miyamoto & Nagai (1975) density 
	CALL MN75(Pa2,Xsm,PMN2,Rdinput,Rsun,MdMN,G,Rgal,DABS(Zgal), PhiMN16, FRMN16, FZMN16) ! Second 
	PoTSm15 = PoTSm15 + PhiMN16    ! Potential 2 from the scaled Miyamoto & Nagai (1975) density 
        FRTSm15 = FRTSm15 + FRMN16     ! Force in R 2 from the scaled Miyamoto & Nagai (1975) density 
	FZTSm15 = FZTSm15 + FZMN16     ! Force in z 1 from the scaled Miyamoto & Nagai (1975) density 
	CALL MN75(Pa3,Xsm,PMN3,Rdinput,Rsun,MdMN,G,Rgal,DABS(Zgal), PhiMN16, FRMN16, FZMN16) ! Third
	PoTSm15 = PoTSm15 + PhiMN16    ! Potential 3 from the scaled Miyamoto & Nagai (1975) density 
        FRTSm15 = FRTSm15 + FRMN16     ! Force in R 3 from the scaled Miyamoto & Nagai (1975) density 
        FZTSm15 = FZTSm15 + FZMN16     ! Force in z 1 from the scaled Miyamoto & Nagai (1975) density 
        RETURN 
        END
