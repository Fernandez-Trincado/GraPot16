[GraPot16](https://github.com/Fernandez-Trincado/GraPot16/blob/master/README.md) in Python version
===

  * [Sech2diskBGM.py](https://github.com/Fernandez-Trincado/GraPot16/blob/master/src/Python/Sech2diskBGM.for) is a Python code created to build the rotation curve, Galactic potential and field force of a Radially exponential disk [Robin et al. (2014)](http://adsabs.harvard.edu/abs/2014A%26A...569A..13R) and/or sech^2 disk [Robin et al. (2014)](http://adsabs.harvard.edu/abs/2014A%26A...569A..13R), based in [Flynn et al. (1996)](http://adsabs.harvard.edu/abs/1996MNRAS.281.1027F), [Smith et al. (2015)](http://adsabs.harvard.edu/abs/2015MNRAS.448.2934S) methods. Fortran code can be found at [Sech2diskBGM.for](https://github.com/Fernandez-Trincado/GraPot16/blob/master/src/Python/Sech2diskBGM.for).

   * Example 1: Rotation curve for a Radially exponential disk, more details in [Robin et al. (2014)](http://adsabs.harvard.edu/abs/2014A%26A...569A..13R).

![RC2](https://github.com/Fernandez-Trincado/GraPot16/blob/master/Figures/PythonRC2016.png)


   * Example 2: Rotation curve for a Radially exponential Interstellar Mattter (ISM). ISM rotation curve from [Robin et al. (2003)](http://adsabs.harvard.edu/abs/2003A%26A...409..523R) in black line, and ISM rotation curve from new physical constraints in orange line (Fernandez-Trincado et al. 2016, in prep.).

![Example2](https://github.com/Fernandez-Trincado/GraPot16/blob/master/Figures/ISM.png)
