GraPot16
==


A 3D fiducial model of the Gravitational Potential of the Milky Way for orbit calculations based on the Besançon Galaxy Model.
------

This repository is maintained by `J. G. Fernandez-Trincado`

Last update: 2016, March 15

Status
---

Scientific Paper in `Journal` 

Galactic Model
---
We have developed a multicomponent model of the Milky Way potential which we have matched with good accuracy the rotation curve, local disk density, ...


  * Axisymmetric Potential
  * Non-axisymmetric Potential

The rotation curve of our model is shown in Figure 1.

![RC](https://github.com/Fernandez-Trincado/GraPot16/blob/master/Figures/RC2016.png)


Author
------

J. G. Fernandez-Trincado - <jfernandez@obs-besancon.fr> and/or <jfernandezt87@gmail.com>

List of contributors.

             - Direct contributions to the code base:
             A. C. Robin 
             A. Perez-Villegas
             O. Bienaymé 
             M. Romero-Gomez
             C. Reylé
             E. Moreno 
             B. Pichardo
             K. Vieira
             O. Valenzuela
             S. DIAKITÉ 

License
---

**Copyright 2016 the authors.** The code in this repository is released under the `open-source UTINAM License`. See the file [LICENSE](https://github.com/Fernandez-Trincado/GraPot16/blob/master/LICENSE) for more details.

In collaboratin with
---

        [1] Institut Utinam, CNRS UMR 6213, Université de Franche-Comté, OSU THETA Franche-Comté-Bourgogne, Observatoire de Be sançon, BP 1615, 25010 Besançon Cedex, France. 
        [2] Instituto de Astronomía, Universidad Nacional Autónoma de México, Apdo. Postal 70264, México D.F., 04510, Mexico.
        [3] Observatoire astronomique de Strasbourg, Universite de Strasbourg, CNRS, UMR 7550, 11 rue de l’Université, F-67000 Stras- bourg, France.
        [4] Max-Planck-Institüt für Extraterrestrische Physik, Gießenbachstraße, 85748 Garching, Germany.


System requirements:
---

* [GraPot16](https://github.com/Fernandez-Trincado/GraPot16/src) work under any Operating Sytem: Mac and Linux

Installation
---


Instructions to run GraPot16 in parallel from Fortran
---

* Definition
  * `threads_numbers`: Number of processors/cores in your local computer or cluster
    * On Mac: `sysctl -n hw.ncpu`
    * On Linux: `cat /proc/cpuinfo | grep processor | wc -l`

 * `Compilation`: gfortran -fopenmp -fbounds-check -fcheck-array-temporaries -Wsurprising -ffixed-line-length-none -o BGM subpot2015c.f boxybar15b.f stellarhalo2015b.f mmbsj0a.for mmbsj1a.for dqsf.for sub_bar.f ecbloc15b.for IC_Main_program.for generador_my.f densitiylaws.f 
  * `Environment configurations`: export OMP_NUM_THREADS = threads_numbers  
  * `Run GraPot16`: ./executable_program_file_here 

`GraPot16` sources:
---
  * Fortran version can be found at [Fortran-GraPot16](https://github.com/Fernandez-Trincado/GraPot16/blob/master/src/Fortran/Readme.md).
  * Python version can be found at [Python-GraPot16](https://github.com/Fernandez-Trincado/GraPot16/tree/master/src/Python). Efforts are underway to provide a Python Package of **GraPot16** to the community.
  * CUDA version can be found at [CUDA-GraPot16](https://github.com/Fernandez-Trincado/GraPot16/tree/master/src/CUDA). Efforts are underway to provide a CUDA Package of **GraPot16** to the community.

Description
---

[GraPot16](https://github.com/Fernandez-Trincado/GraPot16/src) is an extended and updated version of the code previously used by [Bienayme et al. (1987)](http://adsabs.harvard.edu/abs/1987A%26A...180...94B), [Robin et al. (2003)](http://adsabs.harvard.edu/abs/2003A%26A...409..523R), [Pichardo et al. (2004)](http://adsabs.harvard.edu/abs/2004ApJ...609..144P), [Fernandez-Trincado et al. (2015)](http://adsabs.harvard.edu/cgi-bin/nph-data_query?bibcode=2015sf2a.conf...15F&db_key=AST&link_type=ABSTRACT&high=56d0002cfc01206), and [galpy: Bovy et al. (2015)](http://adsabs.harvard.edu/abs/2015ApJS..216...29B) developed under the git version-control system on GitHub.

Support
--

If you use `GraPot16` in your research, we would be grateful if you could include an acknowledgment in papers and/or presentations:

      "This research made use of GraPot16 (XXXXXX version) , a community-developed core under the git version-control system
      on GitHub. Funding for GraPot16 has been provided by Centre National d'Etudes Spatiales (CNES) through 
      grant 0101973 and the R\'egion de Franche-Comt\'e and by the French Programme National de Cosmologie et 
      Galaxies (PNCG)"
      
      Replace XXXXXX with whichever of our products you use: Fortran, Python, or CUDA. For example:
      
      "This research made use of GraPot16 (Fortran version) , a community-developed core under the git version-control system
      on GitHub. Funding for GraPot16 has been provided by Centre National d'Etudes Spatiales (CNES) through 
      grant 0101973 and the R\'egion de Franche-Comt\'e and by the French Programme National de Cosmologie et 
      Galaxies (PNCG)"
      
      
      Thanks. 


GraPot16 Publications 
---

* Publication list here. 


GraPot16 in Conferences 
---

* **EWASS 2016 - European Astronomical Society** On the Threshold of 1st Gaia Data - the Gaia Research for European Astronomy Training ([GREAT](http://great.ast.cam.ac.uk/Greatwiki/GreatMeet-PM9/Symp1Sess5)) Network Science Symposium. 
  * `J. G. Fernandez-Trincado`
  * **Talk:** `The Gravitational Potential (GraPot16) of the Galaxy from the Besançon galaxy model in the Gaia era.`
  * **Abstract:** We will introduce a Three-Dimensional non-axisymmetric fiducial model of the Gravitational Potential (GraPot16) of the Milky Way for orbit calculations based on the Beancon Galactic model.  We extend the work of Robin et al. (2003, 2012, 2014) and develop an extended collection of analytic/semi-analytic gravitational potentials for a double exponential disk with a central hole, boxy bar, hyperbolic secant squared thick disk, combined consistently with the mass model of all components including the dark halo and an additional central mass, in order to produce a more realistic kinematics model for the Besançon Galaxy model. The proposed package of potentials is computationally fast and has the potential to be applied to integrate and characterize orbits to interprete the upcoming six-dimensional phase-space data set produced by Gaia. 

* **SF2A 2016**.
  * `C. Reyle`

